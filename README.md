**Technobabel - 2D Android Shoot 'Em Up**

An incomplete game engine for a 2D shoot 'em up game I worked on in 2010-2011. It was the first big game project I worked on, and also the first Android one. It's written in Java and supports either OpenGL ES or Canvas API, and was meant to work on older Android devices (1.6+). You can read more about it in [this blog post](http://www.firasassaad.com/2011/01/technobabel-technicalities-part-1.html).

In retrospect the code is incomplete and over-engineered (Java does that to you...), but it worked last time I checked and it goes out of its way to avoid garbage collection and do things efficiently. I learned a lot working on this project and it helped me in my [next one](http://octopuscityblues.com).