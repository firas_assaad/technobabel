package com.ghostinabottle.technobabel;

import com.ghostinabottle.technobabel.R;
import com.ghostinabottle.technobabel.graphics.CustomSurfaceView;
import com.ghostinabottle.technobabel.graphics.DisplayManager;
import com.ghostinabottle.technobabel.graphics.DisplaySize;
import com.ghostinabottle.technobabel.graphics.Explosion;
import com.ghostinabottle.technobabel.graphics.GameRenderer;
import com.ghostinabottle.technobabel.graphics.Graphics;
import com.ghostinabottle.technobabel.graphics.SpriteDataFactory;
import com.ghostinabottle.technobabel.graphics.TextureFactory;
import com.ghostinabottle.technobabel.graphics.canvas.CanvasDisplayManager;
import com.ghostinabottle.technobabel.graphics.opengl.GLDisplayManager;
import com.ghostinabottle.technobabel.input.Input;
import com.ghostinabottle.technobabel.logic.CollisionDetector;
import com.ghostinabottle.technobabel.logic.Level;
import com.ghostinabottle.technobabel.object.BulletManager;
import com.ghostinabottle.technobabel.object.GameObject;
import com.ghostinabottle.technobabel.object.GameObjectFactory;
import com.ghostinabottle.technobabel.util.DebugLog;
import com.ghostinabottle.technobabel.util.FixedSizeArray;
import com.ghostinabottle.technobabel.util.GameTime;
import com.ghostinabottle.technobabel.util.ResourceManager;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.os.SystemClock;

/**
 * The Game class. This class runs in its own thread and has the main loop and
 * the various systems and managers. It delegates outside events (such as input)
 * to the appropriate classes and sets up the game.
 */
public final class Game implements Runnable {
	/** The current context (activity). */
	private Context context;
	/** The display manager. */
	private DisplayManager displayManager;
	/** Current level. */
	private Level level;
	/** The input system. */
	private Input input;
	/** The collision detection system. */
	private CollisionDetector collisionDetector;
	/** The player object. */
	private GameObject player;
	/** A factory for allocating sprite data. */
	private SpriteDataFactory spriteDataFactory;
	/** Resource manager. */
	ResourceManager resourceManager;
	/** Is the game paused? */
	private boolean isGamePaused;
	/** Has the game finished? */
	private boolean isGameFinished;
	/** List of explosion effects. */
	private Explosion[] explosions;
	/** Maximum number of game objects. */
	public final static int MAX_NUMBER_OF_OBJECTS = 256;
	/** Number of explosion effects. */
	private final static int NUMBER_OF_EXPLOSIONS = 1;
	/** Logic update FPS. */
	private final static int LOGIC_FPS = 30;
	/** Logic update frame time. */
	private final static int LOGIC_FRAME_TIME = 1000 / LOGIC_FPS;

	public Game(Context context, CustomSurfaceView view, int[] sizes) {
		this.context = context;
		DisplaySize.set(sizes[0], sizes[1], sizes[2], sizes[3]);
		// TODO: Select display manager based on options
		this.displayManager = new CanvasDisplayManager(view, this);
		//this.displayManager = new GLDisplayManager(view, this);
		resourceManager = new ResourceManager(context.getResources(), context.getPackageName());
	}

	/**
	 * Load the different explosions in the explosions array.
	 */
	private void loadExplosions() {
		explosions[0] = new Explosion(R.drawable.explosion, R.raw.explosion);
	}

	/**
	 * The main loop. Updates all the subsystems.
	 */
	public void run() {
		// Wait until the game renderer is ready
		while (!displayManager.getGameRenderer().isReady())
		{
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				
			}
		}
		// Create main game components
		level = new Level(this, context.getResources());
		input = new Input();
		collisionDetector = new CollisionDetector();
		spriteDataFactory = new SpriteDataFactory(context.getResources());
		GameObjectFactory.set(this, MAX_NUMBER_OF_OBJECTS);
		explosions = new Explosion[NUMBER_OF_EXPLOSIONS];
		loadExplosions();
		// Create the player object
		player = GameObjectFactory.createGameObject("player", true);
		int levelId = R.raw.testlevel;
		// Load the level for testing
		level.loadLevel(levelId);

		FixedSizeArray<GameObject> gameObjects = GameObjectFactory
				.getGameObjects();
		long timeSinceUpdate;
		long lastUpdateTime = SystemClock.uptimeMillis();

		// Get the back buffer lock. This thread locks on it when updating
		// logic, while the rendering thread locks on it when swapping buffers
		Object bufferLock = displayManager.getGameRenderer().getBufferLock();
		// The main game loop
		while (!isGameFinished) {
			if (isGamePaused) {
				lastUpdateTime = GameTime.total();
				continue;
			}
			// If not enough time passed since last logic update, wait
			timeSinceUpdate = GameTime.total() - lastUpdateTime;
			if (timeSinceUpdate < LOGIC_FRAME_TIME) {
				continue;
			}
			// Update game logic
			while (timeSinceUpdate >= LOGIC_FRAME_TIME) {
				timeSinceUpdate -= LOGIC_FRAME_TIME;
				synchronized (bufferLock) {
					// Empty the queue of draw commands
					displayManager.clearCommands();
					// Clear screen and update the game components
					displayManager.clearScreen();
					// TODO: Reset level on completion for testing
					if (level.isCompleted()) {
						DebugLog.i("Game", "Restarting level...");
						float x = player.x;
						float y = player.y;
						collisionDetector.reset();
						input.reset();
						GameObjectFactory.set(this, MAX_NUMBER_OF_OBJECTS);
						gameObjects = GameObjectFactory.getGameObjects();
						player = GameObjectFactory.createGameObject("player", true);
						player.x = x;
						player.y = y;
						level.reset();
						level.loadLevel(levelId);
					}
					level.update();
					input.update();
					collisionDetector.update();

					// Update game objects
					int objectCount = gameObjects.getCount();
					for (int i = 0; i < objectCount; i++) {
						gameObjects.get(i).update();
					}

					// Update bullet objects
					BulletManager.update();

				}
			}
			lastUpdateTime = GameTime.total() - timeSinceUpdate;
		}
	}

	/**
	 * Pause the game and notify the game renderer.
	 */
	public void pause() {
		isGamePaused = true;
		displayManager.getGameRenderer().pause();
	}

	/**
	 * Resume the game and notify the game renderer.
	 */
	public void resume() {
		isGamePaused = false;
		displayManager.getGameRenderer().resume();
	}

	/**
	 * Stop the game.
	 */
	public void stop() {
		isGameFinished = true;
		displayManager.getGameRenderer().stop();
	}

	/**
	 * Notify the input watcher of a key down event.
	 *
	 * @param keyCode
	 *            Code of pressed key.
	 * @param event
	 *            Event information.
	 * @return True if event was handled.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		try {
			Thread.sleep(4);
		} catch (InterruptedException e) {
		}
		return input.getInputWatcher().onKeyDown(keyCode, event);
	}

	/**
	 * Notify the input watcher of a key up event.
	 *
	 * @param keyCode
	 *            Code of released key.
	 * @param event
	 *            Event information.
	 * @return True if event was handled.
	 */
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		try {
			Thread.sleep(4);
		} catch (InterruptedException e) {
		}
		return input.getInputWatcher().onKeyUp(keyCode, event);
	}
	
	/**
	 * Notify the input watcher of a touch event.
	 *
	 * @param event
	 *            Event information.
	 * @return True if event was handled.
	 */
	public boolean onTouchEvent(MotionEvent event) {
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
		return input.getInputWatcher().onTouchEvent(event);
	}

	/**
	 * Is the game paused?
	 *
	 * @return True if game is paused.
	 */
	public boolean isPaused() {
		return isGamePaused;
	}

	/**
	 *
	 * @return The current context.
	 */
	public Context getContext() {
		return context;
	}

	/**
	 *
	 * @return The display manager.
	 */
	public DisplayManager getDisplayManager() {
		return displayManager;
	}

	/**
	 *
	 * @return The game renderer.
	 */
	public GameRenderer getGameRenderer() {
		return displayManager.getGameRenderer();
	}

	/**
	 *
	 * @return The texture factory.
	 */
	public TextureFactory getTextureFactory() {
		return displayManager.getTextureFactory();
	}

	/**
	 *
	 * @return The sprite data factory.
	 */
	public SpriteDataFactory getSpriteDataFactory() {
		return spriteDataFactory;
	}

	/**
	 *
	 * @return The graphics system.
	 */
	public Graphics getGraphics() {
		return displayManager.getGraphics();
	}

	/**
	 *
	 * @return The input system.
	 */
	public Input getInput() {
		return input;
	}

	/**
	 *
	 * @return The player object.
	 */
	public GameObject getPlayer() {
		return player;
	}

	/**
	 *
	 * @return The collision detection system.
	 */
	public CollisionDetector getCollisionDetector() {
		return collisionDetector;
	}

	/**
	 *
	 * @param id
	 * @return The explosion with given id.
	 */
	public Explosion getExplosion(int id) {
		assert id >= 0 && id < NUMBER_OF_EXPLOSIONS : "Invalid explosion ID";
		return explosions[id];
	}

	/**
	 *
	 * @return The resource manager.
	 */
	public ResourceManager getResourceManager() {
		return resourceManager;
	}
}
