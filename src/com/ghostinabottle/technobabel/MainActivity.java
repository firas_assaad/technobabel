package com.ghostinabottle.technobabel;

import com.ghostinabottle.technobabel.graphics.CustomSurfaceView;
import com.ghostinabottle.technobabel.graphics.canvas.CanvasSurfaceView;
import com.ghostinabottle.technobabel.graphics.opengl.GLSurfaceView;
import com.ghostinabottle.technobabel.util.DebugLog;
import com.ghostinabottle.technobabel.R;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * The main game activity. Holds the game thread and object.
 */
public class MainActivity extends Activity {
	/** Tag for debugging. */
	private static final String TAG = "MainActivity";
	/** The game thread. */
	private Thread gameThread;
	/** The game runnable. */
	private Game game;
	/** The surface view for the activity. */
	private CustomSurfaceView surfaceView;
	/** Is the game running? */
	private boolean isRunning;

	/**
	 * Called when the activity is first created. Creates the surface view and
	 * starts the game.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO: Select surface view based on options
		CustomSurfaceView view = new CanvasSurfaceView(this);
		//CustomSurfaceView view = new GLSurfaceView(this);
		surfaceView = view;
		// Get width and height
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int gameWidth = 480;
		int gameHeight = 320;
		int[] sizes = { gameWidth, gameHeight, dm.widthPixels, dm.heightPixels };
		game = new Game(this, view, sizes);
		view.setRenderer(game.getGameRenderer());
		setContentView(view);
		start();
	}

	/**
	 * Called when the activity is terminated. Stops the game.
	 */
	@Override
	public void onDestroy() {
		stop();
		DebugLog.d("Main", "Destroyed!");
		super.onDestroy();
	}

	/**
	 * Ignores screen orientation and keyboard changes to stop the activity from
	 * being recreated.
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// Ignore orientation change
		super.onConfigurationChanged(newConfig);
	}

	/**
	 * Called when the activity is paused. Pauses the game and rendering
	 * threads.
	 */
	@Override
	protected void onPause() {
		game.pause();
		surfaceView.onPause();
		super.onPause();
	}

	/**
	 * Called when the activity is resumed. Pauses the game and rendering
	 * threads.
	 */
	@Override
	protected void onResume() {
		super.onResume();
		game.resume();
		surfaceView.onResume();
	}

	/**
	 * Notify the game of the key down event.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return game.onKeyDown(keyCode, event);
	}

	/**
	 * Notify the game of the key up event.
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		return game.onKeyUp(keyCode, event);
	}
	
	/**
	 * Notify the game of the touch event
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return game.onTouchEvent(event);
	}

	/**
	 * Start the game thread.
	 */
	public void start() {
		if (!isRunning) {
			// Run garbage collector
			Runtime r = Runtime.getRuntime();
			r.gc();
			DebugLog.d(TAG, "Starting the game");
			gameThread = new Thread(game);
			gameThread.setName("Game Thread");
			gameThread.start();
			isRunning = true;
		} else {
			game.resume();
		}
	}

	/**
	 * Stop the game and rendering threads.
	 */
	public void stop() {
		if (isRunning) {
			DebugLog.d(TAG, "Stopping the game");

			if (game.isPaused()) {
				game.resume();
			}
			game.stop();
			try {
				gameThread.join();
			} catch (InterruptedException e) {
				gameThread.interrupt();
			}
			surfaceView.stopDrawing();
			gameThread = null;
			isRunning = false;
		}
	}

}