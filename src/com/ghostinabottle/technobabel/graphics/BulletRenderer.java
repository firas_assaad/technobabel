package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.Game;

import android.graphics.Rect;

/**
 * A shared renderer for bullets.
 */
public class BulletRenderer implements Renderer {

	/** Game instance. */
	private Game game;
	/** Bullets texture. */
	private Texture texture;
	/** Source rectangle (in image). */
	private Rect srcRect;
	/** Destination rectangle (on screen). */
	private Rect destRect;

	/**
	 *
	 * @param game
	 *            Game object.
	 * @param id
	 *            Bullet drawable resource ID.
	 */
	public BulletRenderer(Game game, int id) {
		this.game = game;
		this.texture = game.getTextureFactory().allocateTexture(id);
	}

	/**
	 * Set the renderer state. Called before drawing.
	 *
	 * @param srcRect
	 * @param destRect
	 */
	public void set(Rect srcRect, Rect destRect) {
		this.srcRect = srcRect;
		this.destRect = destRect;
	}

	/**
	 * Draw the bullet.
	 */
	@Override
	public void draw() {
		game.getDisplayManager().drawTexture(texture, srcRect, destRect, -1, 0);
	}

}
