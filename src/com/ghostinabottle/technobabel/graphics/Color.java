package com.ghostinabottle.technobabel.graphics;

/** A class for creating and dealing with color values. */
public final class Color {
	public static final int BLACK = 0xFF000000;
    public static final int DARK_GRAY = 0xFF444444;
    public static final int GRAY = 0xFF888888;
    public static final int LIGHT_GRAY = 0xFFCCCCCC;
    public static final int WHITE = 0xFFFFFFFF;
    public static final int RED = 0xFFFF0000;
    public static final int GREEN = 0xFF00FF00;
    public static final int BLUE = 0xFF0000FF;
    public static final int YELLOW = 0xFFFFFF00;
    public static final int CYAN = 0xFF00FFFF;
    public static final int MAGENTA = 0xFFFF00FF;
    public static final int TRANSPARENT = 0;
    
	/**
	 * 
	 * @param color
	 * @return Red component of a 32 bit color
	 */
	public static int red(int color) {
		return (color >> 16) & 0xFF;
	}
	
	/**
	 * 
	 * @param color
	 * @return Green component of a 32 bit color
	 */
	public static int green(int color) {
		return (color >> 8) & 0xFF;
	}
	
	/**
	 * 
	 * @param color
	 * @return Blue component of a 32 bit color
	 */
	public static int blue(int color) {
		return color & 0xFF;
	}
	
	/**
	 * 
	 * @param color
	 * @return Alpha component of a 32 bit color
	 */
	public static int alpha(int color) {
		return color >> 24;
	}
	
	/**
	 * Construct a 32 bit color from red, green, and blue components
	 * @param red
	 * @param green
	 * @param blue
	 * @return A 32 bit color
	 */
	public static int rgb(int red, int green, int blue) {
        return (0xFF << 24) | (red << 16) | (green << 8) | blue;
    }
	
	/**
	 * Construct a 32 bit color from alpha, red, green, and blue components
	 * @param alpha
	 * @param red
	 * @param green
	 * @param blue
	 * @return
	 */
	public static int argb(int alpha, int red, int green, int blue) {
        return (alpha << 24) | (red << 16) | (green << 8) | blue;
    }
	
	
}
