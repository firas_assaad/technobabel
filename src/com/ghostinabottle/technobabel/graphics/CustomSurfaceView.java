package com.ghostinabottle.technobabel.graphics;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

/**
 * The parent class for GLSurfaceView and CanvasSurfaceView serving as a common
 * interface for shared functionality such as queuing events or stopping.
 */
public abstract class CustomSurfaceView extends SurfaceView {

	public CustomSurfaceView(Context context) {
		super(context);
	}

	public CustomSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	/**
	 * Sets the user's renderer and kicks off the rendering thread.
	 * @param renderer
	 */
	public abstract void setRenderer(CustomSurfaceView.Renderer renderer);

	/**
	 * Signal that the game is paused.
	 */
	public abstract void onPause();

	/**
	 * Signal that the game is resumed from pausing.
	 */
	public abstract void onResume();

	/**
	 * Stop the rendering thread.
	 */
	public abstract void stopDrawing();
	
	public static interface Renderer {
		
	}
}
