package com.ghostinabottle.technobabel.graphics;

import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * This important class is responsible for managing the display. It holds
 * display related objects such as the graphics system and the renderer. A
 * Component renderer interfaces with the display manager which in turn sends
 * drawing commands to the renderer's command queue. Specialized into
 * GLDisplayManager and CanvasDisplayManager to create appropriate rendering
 * objects.
 */
public abstract class DisplayManager {
	/** The graphics system. */
	protected Graphics graphics;
	/** The surface view to draw on. */
	protected SurfaceView surfaceView;
	/** A factory for allocating textures. */
	protected TextureFactory textureFactory;
	/** The rendering thread. */
	protected GameRenderer gameRenderer;
	/** A pool of reusable draw commands. */
	private DrawCommandPool pool;
	/** Maximum size of the draw commands pool. */
	public final static int MAX_POOL_SIZE = 255;

	public DisplayManager() {
		pool = new DrawCommandPool(MAX_POOL_SIZE);
	}

	/**
	 * Clear the command queue and return the commands to the pool.
	 */
	public final void clearCommands() {
		gameRenderer.clearCommands(pool);
	}

	/**
	 *
	 * @return The texture factory.
	 */
	public final TextureFactory getTextureFactory() {
		return textureFactory;
	}

	/**
	 *
	 * @return The game renderer.
	 */
	public final GameRenderer getGameRenderer() {
		return gameRenderer;
	}

	/**
	 *
	 * @return The graphics subsystem.
	 */
	public final Graphics getGraphics() {
		return graphics;
	}

	/**
	 *
	 * @return The pool of draw commands.
	 */
	public final DrawCommandPool getDrawCommandPool() {
		return pool;
	}

	/**
	 * Queue a command to clear the screen.
	 */
	public final void clearScreen() {
		DrawCommand command = (DrawCommand) pool.allocate();
		command.setClearCommand(graphics);
		gameRenderer.enqueue(command);
	}

	/**
	 * Queue a command to draw a texture.
	 *
	 * @param texture
	 *            Texture to draw.
	 * @param x
	 *            X coordinate.
	 * @param y
	 *            Y coordinate.
	 */
	public final void drawTexture(Texture texture, int x, int y) {
		DrawCommand command = (DrawCommand) pool.allocate();
		command.setTextureCommand(graphics, texture, x, y);
		gameRenderer.enqueue(command);
	}

	/**
	 * Queue a command to draw a bitmap.
	 *
	 * @param texture
	 * @param sx
	 * @param sy
	 * @param sw
	 * @param sh
	 * @param dx
	 * @param dy
	 * @param dw
	 * @param dh
	 * @param z
	 * @param angle
	 */
	public final void drawTexture(Texture texture, int sx, int sy, int sw, int sh,
			int dx, int dy, int dw, int dh, int z, int angle) {
		DrawCommand command = (DrawCommand) pool.allocate();
		command.setTextureCommand(graphics, texture, sx, sy, sw, sh, dx, dy, dw, dh, z, angle);
		gameRenderer.enqueue(command);
	}

	/**
	 * Queue a command to draw a bitmap.
	 *
	 * @param texture
	 *            Texture to draw.
	 * @param srcRect
	 *            Source rectangle.
	 * @param destRect
	 *            Destination rectangle.
	 * @param z
	 * 	          Z coordinate
	 * @param angle
	 *            Rotation angle.
	 */
	public final void drawTexture(Texture texture, Rect srcRect, Rect destRect, int z, int angle) {
		DrawCommand command = (DrawCommand) pool.allocate();
		command.setTextureCommand(graphics, texture, srcRect, destRect, z, angle);
		gameRenderer.enqueue(command);
	}

	/**
	 * Queue a command to draw a rectangle
	 *
	 * @param x
	 * 			X coordinate of the rectangle
	 * @param y
	 * 			Y coordinate of the rectangle
	 * @param width
	 * 			Width of the rectangle
	 * @param height
	 * 			Height of the rectangle
	 * @param color
	 * 			Color of the rectangle
	 */
	public void drawRect(int x, int y, int width, int height, int color) {
		DrawCommand command = (DrawCommand) pool.allocate();
		command.setRectCommand(graphics, x, y, width, height, color);
		gameRenderer.enqueue(command);
	}
}
