package com.ghostinabottle.technobabel.graphics;

/**
 * A class of static member functions that hold information about the display
 * size.
 */
public final class DisplaySize {

	/** Assumed game width. */
	public static int gameWidth;
	/** Assumed game height. */
	public static int gameHeight;
	/** Actual view width. */
	public static int viewWidth;
	/** Actual view height. */
	public static int viewHeight;
	/** Ratio of view width to game width. */
	public static float viewScaleX;
	/** Ratio of view height to game height. */
	public static float viewScaleY;

	/** Private constructor because we don't need instances of this class. */
	private DisplaySize() {

	}

	/**
	 * Set the values.
	 *
	 * @param gWidth
	 * @param gHeight
	 * @param vWidth
	 * @param vHeight
	 */
	public static void set(int gWidth, int gHeight, int vWidth, int vHeight) {
		viewWidth = vWidth;
		viewHeight = vHeight;
		gameWidth = gWidth;
		gameHeight = gHeight;
		viewScaleX = (float) viewWidth / gameWidth;
		viewScaleY = (float) viewHeight / gameHeight;
	}
	
	/**
	 * Set new view values.
	 *
	 * @param vWidth
	 * @param vHeight
	 */
	public static void set(int vWidth, int vHeight) {
		viewWidth = vWidth;
		viewHeight = vHeight;
		viewScaleX = (float) viewWidth / gameWidth;
		viewScaleY = (float) viewHeight / gameHeight;
	}
}
