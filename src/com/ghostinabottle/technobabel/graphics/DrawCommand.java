package com.ghostinabottle.technobabel.graphics;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.util.Command;

/**
 * A command for drawing something on the screen.
 */
public final class DrawCommand extends Command {
	public enum DrawCommandType {
		CLEAR,
		TEXTURE,
		RECT
	}
	/** Primitive graphics system. */
	private Graphics graphics;
	/** Type of draw command. */
	private DrawCommandType type;
	/** The texture to draw. */
	private Texture texture;
	/** Source rectangle. */
	private Rect srcRect;
	/** Destination rectangle. */
	private Rect destRect;
	/** Rotation angle in degrees or Color of rectangle. */
	private int intParam1;
	/** Layer where this command is drawn. */
	private int layer;

	/**
	 * Default constructor: creates the source and destination rectangles.
	 */
	public DrawCommand() {
		int minValue = Integer.MIN_VALUE;
		srcRect = new Rect(minValue, minValue, minValue, minValue);
		destRect = new Rect(minValue, minValue, minValue, minValue);
		layer = -1;
	}

	/**
	 * Reset command to default state
	 */
	public void reset() {
		graphics = null;
		texture = null;
		int minValue = Integer.MIN_VALUE;
		srcRect.set(minValue, minValue, minValue, minValue);
		destRect.set(minValue, minValue, minValue, minValue);
		intParam1 = 0;
		layer = -1;
	}

	/**
	 * Set the values for a texture command.
	 *
	 * @param graphics
	 *            The underlying graphics system.
	 * @param texture
	 *            The texture to draw.
	 * @param x
	 *            X coordinate.
	 * @param y
	 *            Y coordinate.
	 */
	public void setTextureCommand(Graphics graphics, Texture texture, int x, int y) {
		this.graphics = graphics;
		type = DrawCommandType.TEXTURE;
		this.texture = texture;
		destRect.set(x, y, -1, -1);
		intParam1 = 0;
	}

	/**
	 * Set the values for a texture command.
	 *
	 * @param graphics
	 *            The underlying graphics system.
	 * @param texture
	 *            The texture to draw.
	 * @param sx
	 *            Source X.
	 * @param sy
	 *            Source Y.
	 * @param sw
	 *            Source width.
	 * @param sh
	 *            Source height.
	 * @param dx
	 *            Destination X.
	 * @param dy
	 *            Destination Y.
	 * @param dw
	 *            Destination width.
	 * @param dh
	 *            Destination height.
	 * @param angle
	 *            Rotation angle.
	 */
	public void setTextureCommand(Graphics graphics, Texture texture,
			int sx, int sy, int sw, int sh,
			int dx, int dy, int dw, int dh,
			int layer, int angle) {
		this.graphics = graphics;
		type = DrawCommandType.TEXTURE;
		this.texture = texture;
		srcRect.set(sx, sy, sx + sw, sy + sh);
		destRect.set(dx, dy, dx + dw, dy + dh);
		this.layer = layer;
		intParam1 = angle;
	}

	/**
	 * Set the values for a texture command.
	 *
	 * @param graphics
	 *            The underlying graphics system.
	 * @param texture
	 *            The texture to draw.
	 * @param srcRect
	 *            Source rectangle.
	 * @param destRect
	 *            Destination rectangle.
	 * @param angle
	 *            Rotation angle.
	 */
	public void setTextureCommand(Graphics graphics, Texture texture, Rect srcRect,
			Rect destRect, int layer, int angle) {
		this.graphics = graphics;
		type = DrawCommandType.TEXTURE;
		this.texture = texture;
		this.srcRect.set(srcRect);
		this.destRect.set(destRect);
		this.layer = layer;
		intParam1 = angle;
	}

	/**
	 * Set the values for a clear command.
	 *
	 * @param graphics
	 *            The underlying graphics system.
	 */
	void setClearCommand(Graphics graphics) {
		this.graphics = graphics;
		type = DrawCommandType.CLEAR;
		layer = 0;
	}


	/**
	 * Set the values for a rectangle command
	 * @param graphics
	 * 			The underlying graphics system.
	 * @param x
	 * 			X coordinate of the rectangle
	 * @param y
	 * 			Y coordinate of the rectangle
	 * @param width
	 * 			Width of the rectangle
	 * @param height
	 * 			Height of the rectangle
	 * @param color
	 * 			Color of the rectangle
	 */
	void setRectCommand(Graphics graphics, int x, int y, int width, int height, int color) {
		this.graphics = graphics;
		type = DrawCommandType.RECT;
		destRect.set(x, y, x + width, y + height);
		intParam1 = color;
	}

	@Override
	public void process() {
		switch (type) {
		case TEXTURE:
			if (destRect.right == Integer.MIN_VALUE)
				graphics.drawTexture(texture, destRect.left, destRect.top);
			else {
				// Check angle
				if (intParam1 == 0)
					graphics.drawTexture(texture, srcRect, destRect, layer);
				else
					graphics.drawTexture(texture, srcRect, destRect, intParam1, layer);
			}

			break;
		case  CLEAR:
			graphics.clear();
			break;
		case RECT:
			graphics.drawRect(destRect, intParam1);
			break;
		}
	}
	
	/**
	 * Some commands (such as drawing textures) apply to specific layers
	 * @return Drawing layer
	 */
	public int getLayer() {
		if (layer > -1)
			return layer;
		else
			return GameRenderer.MAX_NUMBER_OF_LAYERS - 1;
	}

}
