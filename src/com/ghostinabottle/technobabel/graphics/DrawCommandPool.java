package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.object.ObjectPool;

/** A pool for allocating draw commands. */
public class DrawCommandPool extends ObjectPool {

	/**
	 * Create pool with default size.
	 */
	public DrawCommandPool() {
	}

	/**
	 * Create pools with given maximum size.
	 *
	 * @param size
	 *            Maximum size for pools.
	 */
	public DrawCommandPool(int size) {
		super(size);
	}

	@Override
	public void release(Object entry) {
		((DrawCommand)entry).reset();
		super.release(entry);
	}

	/**
	 * Fill the pool with empty bitmap draw commands.
	 */
	@Override
	protected void fill() {
		int size = getSize();
		for (int i = 0; i < size; i++) {
			getAvailable().add(new DrawCommand());
		}
	}
}
