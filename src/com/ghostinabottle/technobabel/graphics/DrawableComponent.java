package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.logic.GameComponent;
import com.ghostinabottle.technobabel.object.GameObject;

/**
 * A game component that can be drawn such as a sprite or a background.
 */
public abstract class DrawableComponent extends GameComponent {

	public DrawableComponent(GameObject object) {
		super(object);
	}

	/**
	 * Do nothing.
	 */
	@Override
	public void update() {
		this.draw();
	}

	/**
	 * Draw the component (usually using its renderer).
	 */
	public abstract void draw();

}
