package com.ghostinabottle.technobabel.graphics;

/**
 * Information about an explosion sprite.
 */
public final class Explosion {
	/** The ID of the sprite's bitmap. */
	public int textureId;
	/** The ID of the sprite's data. */
	public int spriteId;

	public Explosion(int textureId, int spriteId) {
		this.textureId = textureId;
		this.spriteId = spriteId;
	}
}
