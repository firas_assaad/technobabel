package com.ghostinabottle.technobabel.graphics;

/** A single frame in a pose. */
public final class Frame {
	/** Frame duration in milliseconds. */
	public int duration;
	/** X offset in sprite sheet. */
	public int x;
	/** Y offset in sprite sheet. */
	public int y;
	/** Width of the frame. */
	public int width;
	/** Height of the frame. */
	public int height;
	/** Horizontal magnification. */
	public float xMagnification;
	/** Vertical magnification. */
	public float yMagnification;
	/** Rotation angle. */
	public int angle;
	/** Is it a tween frame? */
	public boolean isTweenFrame;

	public Frame() {
		duration = -1;
		xMagnification = yMagnification = 1.0f;
	}

}
