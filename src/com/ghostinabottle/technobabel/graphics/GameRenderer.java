package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.graphics.canvas.CanvasSurfaceView;
import com.ghostinabottle.technobabel.graphics.opengl.GLSurfaceView;
import android.os.SystemClock;

/**
 * Game Renderer is called in the rendering thread by the surface views to draw
 * things. It holds a queue of drawing commands which are processed when drawing
 * a frame.
 */
public abstract class GameRenderer implements GLSurfaceView.Renderer,
		CanvasSurfaceView.Renderer {
	/** Maximum number of layers. */
	public final static int MAX_NUMBER_OF_LAYERS = 5;
	/** Last frame time in milliseconds. */
	protected long lastFrameTime;
	/** Is rendering paused? */
	protected boolean isPaused;
	/** The display manager that owns this renderer. */
	protected DisplayManager displayManager;
	/** Lock for drawing to the back buffer. */
	protected Object bufferLock;
	/** Is the renderer ready to be used? */
	protected boolean ready;

	public GameRenderer(DisplayManager displayManager) {
		this.displayManager = displayManager;
		lastFrameTime = SystemClock.uptimeMillis();
		bufferLock = new Object();
	}

	/**
	 * Called when exiting the game.
	 */
	public final void stop() {

	}

	/**
	 *
	 * @return Last frame time in milliseconds.
	 */
	public final long getFrameTime() {
		return lastFrameTime;
	}

	/**
	 * Enqueue the command into the appropriate back queue.
	 * @param command
	 */
	public abstract void enqueue(DrawCommand command);

	/**
	 * Clear the commands in the queue and release them to the pool.
	 * @param pool
	 */
	public abstract void clearCommands(DrawCommandPool pool);

	/**
	 *
	 * @return The backbuffer lock.
	 */
	public final Object getBufferLock() {
		return bufferLock;
	}

	/**
	 * Pause rendering.
	 */
	public final void pause() {
		isPaused = true;
	}

	/**
	 * Resume rendering.
	 */
	public final void resume() {
		isPaused = false;
	}
	
	/** 
	 * 
	 * @return true if the renderer is ready.
	 */
	public boolean isReady() {
		return ready;
	}

}
