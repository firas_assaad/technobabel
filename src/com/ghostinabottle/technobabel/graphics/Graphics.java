package com.ghostinabottle.technobabel.graphics;

import android.graphics.Color;
import android.graphics.Rect;

/**
 * The graphics system used to do primitive drawing. Subclassed to do the actual
 * work.
 */
public abstract class Graphics {

	/** Screen clear color. */
	public static final int CLEAR_COLOR = Color.RED;

	/**
	 * Draw a texture at the given coordinates.
	 *
	 * @param texture
	 *            Texture to draw.
	 * @param x
	 *            X coordinate.
	 * @param y
	 *            Y coordinate.
	 */
	public abstract void drawTexture(Texture texture, int x, int y);

	public abstract void drawRect(Rect rect, int color);

	/**
	 * Draw texture with given source and destination rectangles.
	 *
	 * @param texture
	 *            Texture to draw.
	 * @param srcRect
	 *            Destination rectangle on the screen.
	 * @param destRect
	 *            Source rectangle in the image.
	 * @param layer
	 *            Layer to draw on.
	 */
	public abstract void drawTexture(Texture texture, Rect srcRect,
			Rect destRect, int layer);

	/**
	 * Draw texture with given source and destination rectangles.
	 *
	 * @param texture
	 *            Texture to draw.
	 * @param srcRect
	 *            Destination rectangle on the screen.
	 * @param destRect
	 *            Source rectangle in the image.
	 * @param angle
	 *            Rotation angle.
	 * @param layer
	 *            Layer to draw on. 
	 */
	public abstract void drawTexture(Texture texture, Rect srcRect,
			Rect destRect, int angle, int layer);

	/**
	 * Clear the screen to the clear color.
	 */
	public abstract void clear();
}
