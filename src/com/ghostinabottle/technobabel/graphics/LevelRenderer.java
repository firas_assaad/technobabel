package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.logic.BackgroundSprite;
import com.ghostinabottle.technobabel.logic.Level;
import com.ghostinabottle.technobabel.logic.Scene;

/**
 * Class responsible for rendering level backgrounds.
 *
 */
public class LevelRenderer implements Renderer {
	/** Level to render. */
	private Level level;

	public LevelRenderer(Level level) {
		this.level = level;
	}

	@Override
	public void draw() {
		Scene scene = level.getCurrentScene();
		int numSprites = scene.sprites.getCount();
		for (int i = 0; i < numSprites; i++) {
			BackgroundSprite sprite = scene.sprites.get(i);
			SpriteRenderer renderer = sprite.renderer;
			if (sprite.isBackground) {
				// Draw exiting
				renderer.draw(sprite.x, sprite.y, sprite.layer);
				// Draw entering
				renderer.draw(sprite.x + DisplaySize.gameWidth, sprite.y, sprite.layer);
		}
			else {
				renderer.draw(sprite.x, sprite.y, sprite.layer);
			}
			sprite.x += sprite.xVelocity;
			sprite.y += sprite.yVelocity;
			if (sprite.x + renderer.getTextureWidth() <= 0) {
				if (sprite.isBackground) {
					sprite.x = 0;
				} else {
					sprite.x = DisplaySize.gameWidth;
				}
			}
		}
	}

}
