package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.logic.BoundingBox;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/** A single pose in a sprite. */
public final class Pose {
	/** Name of the pose (e.g. 'attacking). */
	public String name;
	/** Collision bounding box. */
	public BoundingBox boundingBox;
	/** Total duration of one pose cycle in milliseconds. */
	public int defaultDuration;
	/** Number of times pose is repeated (-1 = forever). */
	public int repeats;
	/** Maximum number of frames in a pose. */
	public final static int MAX_POSE_FRAMES = 8;
	/** List of frames. */
	public FixedSizeArray<Frame> frames;

	/** Default constructor. */
	public Pose() {
		boundingBox = new BoundingBox(-1, -1, -1, -1);
		defaultDuration = 100;
		frames = new FixedSizeArray<Frame>(MAX_POSE_FRAMES);
	}

}
