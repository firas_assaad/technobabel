package com.ghostinabottle.technobabel.graphics;

/** A renderer is something that can be drawn. */
public interface Renderer {
	/** Draw the object represented by the renderer. */
	void draw();
}
