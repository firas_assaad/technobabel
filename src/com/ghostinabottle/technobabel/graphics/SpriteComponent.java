package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.logic.SpriteLogic;
import com.ghostinabottle.technobabel.object.BasicObject;
import com.ghostinabottle.technobabel.object.GameObject;
import com.ghostinabottle.technobabel.util.ResourceManager;

/**
 * An (optionally) animated sprite that can be attached to an object.
 */
public class SpriteComponent extends DrawableComponent {
	/** The texture used for drawing. */
	private Texture texture;
	/** Sprite updater. */
	private SpriteLogic spriteLogic;
	/** The sprite's renderer. */
	private SpriteRenderer renderer;
	/** Did the explosion animation start? */
	private boolean startedExploding;

	/**
	 * Create a sprite from sprite data resource IDs.
	 *
	 * @param object
	 *            Object that owns this sprite.
	 * @param spriteId
	 *            Resource ID for the sprite data XML file.
	 */
	public SpriteComponent(GameObject object, int spriteId) {
		super(object);
		DisplayManager manager = object.getGame().getDisplayManager();
		renderer = new SpriteRenderer(manager);
		set(spriteId);
	}

	/**
	 * Set the texture and sprite data resource IDs.
	 *
	 * @param spriteId
	 *            Resource ID for the sprite data XML file.
	 */
	public final void set(int spriteId) {
		Game game = basicObject.getGame();
		if (texture != null)
			game.getTextureFactory().releaseTexture(texture);
		SpriteDataFactory spriteFactory = game.getSpriteDataFactory();
		SpriteData spriteData = spriteFactory.allocateSpriteData(spriteId);
		ResourceManager rm = basicObject.getGame().getResourceManager();
		int imageId = rm.getResource(spriteData.image, "drawable");
		texture = game.getTextureFactory().allocateTexture(imageId);
		// TODO: Don't create new sprite logic for explosions (recycle)
		spriteLogic = new SpriteLogic(spriteData);
		setPose("Main");
		renderer.set(spriteLogic, texture);
	}

	public void setPose(String poseName) {
		spriteLogic.setPose(poseName);
		basicObject.boundingBox = spriteLogic.getBoundingBox();
		basicObject.width = spriteLogic.getSourceRect().width();
		basicObject.height = spriteLogic.getSourceRect().height();
	}

	/**
	 * Release the texture back to the texture factory.
	 */
	public void release() {
		basicObject.getGame().getTextureFactory().releaseTexture(texture);
	}

	/**
	 * Ask the sprite renderer to draw itself.
	 */
	@Override
	public void draw() {
		// If object is exploding switch to the associated explosion sprite
		if (basicObject.isExploding && !startedExploding) {
			startedExploding = true;
			int id = spriteLogic.getSpriteData().explosionId;
			Explosion explosion = basicObject.getGame().getExplosion(id);
			set(explosion.spriteId);
		}
		// Check if explosion animation is done.
		if (startedExploding) {
			if (spriteLogic.isFinished()) {
				basicObject.isDead = true;
				return;
			}
		}

		spriteLogic.update();
		renderer.draw((int) basicObject.x, (int) basicObject.y, basicObject.layer);
	}

	/**
	 *
	 * @return The sprite's texture.
	 */
	public final Texture getTexture() {
		return texture;
	}

	/**
	 *
	 * @return The sprite updater
	 */
	public final SpriteLogic getSpriteLogic() {
		return spriteLogic;
	}

	/**
	 *
	 * @return The game object that owns the sprite.
	 */
	public final BasicObject getGameObject() {
		return basicObject;
	}

}
