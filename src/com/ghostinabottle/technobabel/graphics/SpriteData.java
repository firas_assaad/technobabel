package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * A container of poses. SpriteData only contains data read from XML files and
 * not any actual logic, which is represented by SpriteLogic.
 */
public class SpriteData {
	/** Maximum number of poses in a sprite. */
	public final static int MAX_SPRITE_POSES = 3;
	/** Name of the sprite image. */
	public String image;
	/** List of poses. */
	public FixedSizeArray<Pose> poses;
	/** ID of explosion effect. */
	public int explosionId;

	public SpriteData() {
		poses = new FixedSizeArray<Pose>(MAX_SPRITE_POSES);
	}
}
