package com.ghostinabottle.technobabel.graphics;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.SAXException;

import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.util.Xml;
import android.util.Xml.Encoding;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.util.DebugLog;
import com.ghostinabottle.technobabel.util.HashTable;
import com.ghostinabottle.technobabel.util.SpriteSaxHandler;

/**
 * A factory for creating SpriteData out of XML files.
 *
 */
public final class SpriteDataFactory {
	/** Resources object to access XML resource. */
	private Resources resources;
	/** Hash table for reusing sprite data. */
	private HashTable<Integer, SpriteData> cache;
	/** SAX handler for parsing the XML file. */
	private SpriteSaxHandler handler;

	/**
	 * Create the sprite data factory.
	 *
	 * @param resources
	 */
	public SpriteDataFactory(Resources resources) {
		this.resources = resources;
		cache = new HashTable<Integer, SpriteData>(Game.MAX_NUMBER_OF_OBJECTS);
		handler = new SpriteSaxHandler();
	}

	/**
	 * Allocate and return a sprite data object.
	 *
	 * @param spriteId
	 *            Resource ID of the XML file.
	 * @return The sprite data for the specified spriteID.
	 */
	public SpriteData allocateSpriteData(int spriteId) {
		// Check if data was already allocated and is in the cache
		SpriteData spriteData = cache.get(spriteId);
		// If not, parse the XML file and add the data to the cache
		if (spriteData == null) {
			InputStream istream = resources.openRawResource(spriteId);
			try {
				Xml.parse(istream, Encoding.UTF_8, handler);
			} catch (NotFoundException e) {
				DebugLog.e("SpriteDataFactory", "Sprite XML file " +
						spriteId + " not found.");
			} catch (IOException e) {
				DebugLog.e("SpriteDataFactory", "IO Exception when opening " +
						"level XML file " +  spriteId + " - " + e.getMessage());
			} catch (SAXException e) {
				DebugLog.e("SpriteDataFactory", "SAX Exception when opening "
						+ "level XML file " + spriteId + " - " + e.getMessage());
			}
			spriteData = handler.getSpriteData();
			cache.put(spriteId, spriteData);
		}

		return spriteData;
	}
}
