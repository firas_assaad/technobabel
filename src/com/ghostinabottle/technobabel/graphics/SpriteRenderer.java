package com.ghostinabottle.technobabel.graphics;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.logic.SpriteLogic;

/**
 * A sprite renderer is a special renderer that draws a sprite.
 */
public final class SpriteRenderer {

	/** The display manager. */
	private DisplayManager displayManager;
	/** Sprite to draw. */
	private Texture texture;
	private SpriteLogic spriteLogic;

	public SpriteRenderer(DisplayManager dm) {
		displayManager = dm;
	}

	/**
	 * Set sprite logic and texture
	 * @param logic
	 * @param tex
	 */
	public void set(SpriteLogic logic, Texture tex) {
		spriteLogic = logic;
		texture = tex;
	}

	/**
	 * Set a custom source rectangle.
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void setSourceRect(int x, int y, int width, int height) {
		spriteLogic.setSourceRect(x, y, width, height);
	}

	/** Draw the sprite's bitmap on screen. */
	public void draw(int x, int y, int z) {
		Rect srcRect = spriteLogic.getSourceRect();
		Frame currentFrame = spriteLogic.getCurrentFrame();
		float xMag = currentFrame.xMagnification;
		float yMag = currentFrame.yMagnification;
		int angle = currentFrame.angle;
		int destWidth = (int) (xMag * srcRect.width());
		int destHeight = (int) (yMag * srcRect.height());
		displayManager.drawTexture(texture,
				srcRect.left, srcRect.top, srcRect.width(), srcRect.height(),
				x, (int) y, destWidth, destHeight,
				z, angle);
	}

	public SpriteLogic getSpriteLogic() {
		return spriteLogic;
	}

	public Texture getTexture() {
		return texture;
	}
	
	public int getTextureWidth() {
		return texture.getWidth();
	}
	
	public int getTextureHeight() {
		return texture.getHeight();
	}
}