package com.ghostinabottle.technobabel.graphics;

/** Abstraction for OpenGL's textures and canvas bitmaps. */
public abstract class Texture {
	/**
	 * 
	 * @return Width of the texture.
	 */
	public abstract int getWidth();

	/**
	 * 
	 * @return Height of the texture.
	 */
	public abstract int getHeight();

	/**
	 * 
	 * @return Texture ID.
	 */
	public abstract int getID();
}
