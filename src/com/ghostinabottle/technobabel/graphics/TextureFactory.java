package com.ghostinabottle.technobabel.graphics;

import com.ghostinabottle.technobabel.Game;

/**
 * Abstract texture factory for managing textures.
 */
public abstract class TextureFactory {

	/** Game instance. */
	protected Game game;

	public TextureFactory(Game game) {
		this.game = game;
	}

	/**
	 * Allocate a texture.
	 *
	 * @param id
	 *            Resource ID.
	 * @return The allocated texture.
	 */
	public abstract Texture allocateTexture(int id);

	/**
	 * Release the texture.
	 *
	 * @param texture
	 *            Texture to release.
	 */
	public abstract void releaseTexture(Texture texture);
}
