package com.ghostinabottle.technobabel.graphics.canvas;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.CustomSurfaceView;
import com.ghostinabottle.technobabel.graphics.DisplayManager;

/**
 * A display manager that uses Android canvas for rendering. The main use of
 * this class is to create Canvas specific classes such as CanvasGraphics.
 */
public class CanvasDisplayManager extends DisplayManager {

	/**
	 *
	 * @param view
	 *            The view to draw on.
	 */
	public CanvasDisplayManager(CustomSurfaceView view, Game game) {
		surfaceView = view;
		textureFactory = new CanvasTextureFactory(game);
		graphics = new CanvasGraphics();
		gameRenderer = new CanvasGameRenderer(this);
	}



}
