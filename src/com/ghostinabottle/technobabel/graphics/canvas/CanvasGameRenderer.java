package com.ghostinabottle.technobabel.graphics.canvas;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Color;
import com.ghostinabottle.technobabel.graphics.DisplayManager;
import com.ghostinabottle.technobabel.graphics.DisplaySize;
import com.ghostinabottle.technobabel.graphics.DrawCommand;
import com.ghostinabottle.technobabel.graphics.DrawCommandPool;
import com.ghostinabottle.technobabel.graphics.GameRenderer;
import com.ghostinabottle.technobabel.util.CommandQueue;
import com.ghostinabottle.technobabel.util.FixedSizeArray;
import com.ghostinabottle.technobabel.util.GameTime;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 *
 * A GameRenderer that uses Canvas to draw.
 */
public final class CanvasGameRenderer extends GameRenderer {
	/** For canvas primitive rendering. */
	private CanvasGraphics graphics;
	/** Drawing is retained on this bitmap between onDrawFrame calls. */
	private Bitmap bitmap;
	/** Canvas used to draw on the bitmap. */
	private Canvas drawCanvas;
	/** Source rectangle for drawing bitmap. */
	private Rect srcRect;
	/** Destination rectangle for screen bitmap. */
	private Rect destRect;
	/** Paint for drawing text. */
	private Paint textPaint;
	/** Paint for resizing the bitmap. */
	private Paint drawPaint;
	/** Queues of drawing commands being rendered. */
	private FixedSizeArray<CommandQueue> frontQueues;
	/** Queues of drawing commands being populated */
	private FixedSizeArray<CommandQueue> backQueues;

	/**
	 *
	 * @param manager
	 *            Display manager.
	 */
	public CanvasGameRenderer(DisplayManager manager) {
		super(manager);
		graphics = (CanvasGraphics) displayManager.getGraphics();
		bitmap = Bitmap.createBitmap(
				DisplaySize.gameWidth, 
				DisplaySize.gameHeight,
				Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(bitmap);
		graphics.setCanvas(drawCanvas);
		srcRect = new Rect(0, 0, DisplaySize.gameWidth, DisplaySize.gameHeight);
		destRect = new Rect(0, 0, DisplaySize.viewWidth, DisplaySize.viewHeight);
		textPaint = new Paint();
		textPaint.setColor(Color.WHITE);
		textPaint.setShadowLayer(1, 0, 0, Color.BLACK);
		drawPaint = new Paint();
		drawPaint.setFilterBitmap(true);
		ready = true;
		frontQueues = new FixedSizeArray<CommandQueue>(MAX_NUMBER_OF_LAYERS);
		backQueues = new FixedSizeArray<CommandQueue>(MAX_NUMBER_OF_LAYERS);
		for (int i = 0; i < MAX_NUMBER_OF_LAYERS; i++) {
			frontQueues.add(new CommandQueue(Game.MAX_NUMBER_OF_OBJECTS));
			backQueues.add(new CommandQueue(Game.MAX_NUMBER_OF_OBJECTS));
		}
	}

	/**
	 * This is only used in a GLGameRenderer. It is still present here to unify
	 * the different renderer interfaces.
	 */
	@Override
	public void onDrawFrame(GL10 gl) {
		// Do nothing!
	}

	/**
	 * This is only used in a GLGameRenderer. It is still present here to unify
	 * the different renderer interfaces.
	 */
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		// Do nothing!
	}

	/**
	 * This is only used in a GLGameRenderer. It is still present here to unify
	 * the different renderer interfaces.
	 */
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// Do nothing!
	}

	/**
	 * Process the command queue for pending rendering commands and release them
	 * back to the draw commands pool.
	 */
	@Override
	public void onDrawFrame(Canvas canvas) {
		if (isPaused || bitmap == null) {
			return;
		}
		// Process commands in the frontQueue
		DrawCommand command;
		for (int i = 0; i < MAX_NUMBER_OF_LAYERS; i++) {
			CommandQueue queue = frontQueues.get(i);
			while (!queue.empty()) {
				command = (DrawCommand) queue.dequeue();
				command.process();
				displayManager.getDrawCommandPool().release(command);
			}
		}
		// Try to swap buffers if logic isn't updating the back buffer
		synchronized (bufferLock) {
			FixedSizeArray<CommandQueue> temp = frontQueues;
			frontQueues = backQueues;
			backQueues = temp;
		}
		// All drawing happens to a bitmap which is then copied to the surface.
		// I used to draw directly on the surface and got weird flickering.
		canvas.drawBitmap(bitmap, srcRect, destRect, drawPaint);
		//canvas.drawText("FPS: " + GameTime.fps, 10, 15, textPaint);

		// Update frame counter
		lastFrameTime = GameTime.total();
		GameTime.update();
	}

	/**
	 * Create a bitmap to draw to based on supplied width and height.
	 */
	@Override
	public void sizeChanged(int width, int height) {
		if (width == 0 || height == 0)
			return;
		DisplaySize.set(width, height);
		destRect.set(0, 0, DisplaySize.viewWidth, DisplaySize.viewHeight);
	}

	@Override
	public void clearCommands(DrawCommandPool pool) {
		for (int i = 0; i < MAX_NUMBER_OF_LAYERS; i++) {
			CommandQueue queue = backQueues.get(i);
			while (!queue.empty()) {
				DrawCommand command = (DrawCommand) queue.dequeue();
				pool.release(command);
			}
		}
	}

	@Override
	public void enqueue(DrawCommand command) {
		backQueues.get(command.getLayer()).enqueue(command);
	}

}
