package com.ghostinabottle.technobabel.graphics.canvas;

import com.ghostinabottle.technobabel.graphics.Graphics;
import com.ghostinabottle.technobabel.graphics.Texture;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;

/**
 * A class for primitive canvas rendering.
 */
public class CanvasGraphics extends Graphics {
	/** The canvas to draw on. */
	private Canvas canvas;
	/** The paint object for drawing rectangles. */
	private Paint paint;

	/**
	 * Set the drawing canvas.
	 *
	 * @param canvas
	 *            The canvas to draw on.
	 */
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	@Override
	public void clear() {
		canvas.drawColor(CLEAR_COLOR, PorterDuff.Mode.SRC);
	}

	@Override
	public void drawTexture(Texture texture, int x, int y) {
		CanvasTexture canvasTexture = (CanvasTexture) texture;
		canvas.drawBitmap(canvasTexture.getBitmap(), x, y, null);
	}

	@Override
	public void drawTexture(Texture texture, Rect srcRect, Rect destRect, int layer) {
		CanvasTexture canvasTexture = (CanvasTexture) texture;
		canvas.drawBitmap(canvasTexture.getBitmap(), srcRect, destRect, null);
	}

	@Override
	public void drawTexture(Texture texture, Rect srcRect, Rect destRect,
			int angle, int layer) {
		canvas.save();
		int midX = destRect.left + destRect.width() / 2;
		int midY = destRect.top + destRect.height() / 2;
		canvas.rotate(angle, midX, midY);
		drawTexture(texture, srcRect, destRect, layer);
		canvas.restore();
	}

	@Override
	public void drawRect(Rect rect, int color) {
		if (paint == null)
			paint = new Paint();
		paint.setColor(color);
		canvas.drawRect(rect, paint);
	}

}
