package com.ghostinabottle.technobabel.graphics.canvas;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Texture;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * A canvas texture just holds a bitmap and its resource ID.
 */
public class CanvasTexture extends Texture {
	/** Bitmap represented by the texture. */
	private Bitmap bitmap;
	/** The resource ID for the bitmap. */
	private int textureID;

	/**
	 * Load the bitmap with the given resource ID.
	 * 
	 * @param game
	 *            Game object (for the resources).
	 * @param id
	 *            Bitmap resource ID.
	 */
	public CanvasTexture(Game game, int id) {
		textureID = id;
		bitmap = BitmapFactory.decodeResource(
				game.getContext().getResources(), id);
	}

	@Override
	public int getHeight() {
		return bitmap.getHeight();
	}

	@Override
	public int getWidth() {
		return bitmap.getWidth();
	}

	/**
	 * For a canvas texture the ID is just the resource ID.
	 */
	@Override
	public int getID() {
		return textureID;
	}

	/** Get the bitmap loaded from the resource ID. */
	public Bitmap getBitmap() {
		return bitmap;
	}

}
