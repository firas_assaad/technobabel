package com.ghostinabottle.technobabel.graphics.canvas;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Texture;
import com.ghostinabottle.technobabel.graphics.TextureFactory;

/**
 * Creates and manages Canvas textures.
 */
public class CanvasTextureFactory extends TextureFactory {

	public CanvasTextureFactory(Game game) {
		super(game);
	}

	/** Allocates a new canvas texture and returns it. */
	@Override
	public Texture allocateTexture(int id) {
		Texture texture = new CanvasTexture(game, id);
		return texture;
	}

	/**
	 * For canvas textures we don't worry about releasing, but I might add a
	 * texture pool later if needed.
	 */
	@Override
	public void releaseTexture(Texture texture) {
		// do nothing
	}

}
