package com.ghostinabottle.technobabel.graphics.opengl;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.CustomSurfaceView;
import com.ghostinabottle.technobabel.graphics.DisplayManager;

/**
 * A display manager that uses OpenGL ES for rendering. The main use of this
 * class is to create GL specific classes such as GLGraphics.
 */
public class GLDisplayManager extends DisplayManager {

	public GLDisplayManager(CustomSurfaceView view, Game game) {
		surfaceView = view;
		textureFactory = new GLTextureFactory(game);
		graphics = new GLGraphics();
		gameRenderer = new GLGameRenderer(this);
	}

}
