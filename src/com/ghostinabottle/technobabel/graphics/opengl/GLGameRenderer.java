package com.ghostinabottle.technobabel.graphics.opengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Color;
import com.ghostinabottle.technobabel.graphics.DisplayManager;
import com.ghostinabottle.technobabel.graphics.DisplaySize;
import com.ghostinabottle.technobabel.graphics.DrawCommand;
import com.ghostinabottle.technobabel.graphics.DrawCommandPool;
import com.ghostinabottle.technobabel.graphics.GameRenderer;
import com.ghostinabottle.technobabel.graphics.Graphics;
import com.ghostinabottle.technobabel.util.CommandQueue;
import com.ghostinabottle.technobabel.util.DebugLog;
import com.ghostinabottle.technobabel.util.GameTime;

import android.graphics.Canvas;

/**
 *
 * A GameRenderer that uses OpenGL ES to draw.
 */
public final class GLGameRenderer extends GameRenderer {
	/** For canvas primitive rendering. */
	private GLGraphics graphics;
	/** Queue of drawing commands being rendered. */
	private CommandQueue frontQueue;
	/** Queue of drawing commands being populated */
	private CommandQueue backQueue;
	
	public GLGameRenderer(DisplayManager manager) {
		super(manager);
		graphics = (GLGraphics) manager.getGraphics();
		frontQueue = new CommandQueue(Game.MAX_NUMBER_OF_OBJECTS);
		backQueue = new CommandQueue(Game.MAX_NUMBER_OF_OBJECTS);
	}

	/**
	 * Process the command queue for pending rendering commands and release them
	 * back to the draw commands pool.
	 */
	@Override
	public void onDrawFrame(GL10 gl) {
		if (isPaused) {
			return;
		}
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		
		// Process commands in the frontQueue
		DrawCommand command;;
		while (!frontQueue.empty()) {
			command = (DrawCommand) frontQueue.dequeue();
			command.process();
			displayManager.getDrawCommandPool().release(command);
		}
		
		// Try to swap buffers if logic isn't updating the back buffer
		synchronized (bufferLock) {
			CommandQueue temp = frontQueue;
			frontQueue = backQueue;
			backQueue = temp;
		}

		// Update frame counter
		lastFrameTime = GameTime.total();
		GameTime.update();
	}

	/**
	 * Called when the surface changes. Setup viewport and projection
	 */
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		DebugLog.d("GLGameRenderer", "Surface size change: " + width + ", " + height);
		
		// Set the GL instance
		GLTextureFactory.setGL(gl);
		graphics.setGl(gl);
        
		// Set the view port
		DisplaySize.set(width, height);
    	final int viewportWidth = (int) (DisplaySize.gameWidth * DisplaySize.viewScaleX);
    	final int viewportHeight = (int) (DisplaySize.gameHeight * DisplaySize.viewScaleY);
        gl.glViewport(0, 0, viewportWidth, viewportHeight);

        
        // Set the projection matrix
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, DisplaySize.gameWidth, DisplaySize.gameHeight, 0, 0, MAX_NUMBER_OF_LAYERS);
        
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        ready = true;
	}

	/**
	 * Called when the surface is created.
	 */
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		int color = Graphics.CLEAR_COLOR;
		gl.glClearColor(Color.red(color), Color.green(color), Color.blue(color), 1.0f);
		gl.glShadeModel(GL10.GL_FLAT);
		gl.glDisable(GL10.GL_DITHER);
        gl.glDisable(GL10.GL_LIGHTING);
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glEnable(GL10.GL_ALPHA_TEST);
        gl.glAlphaFunc(GL10.GL_NOTEQUAL, 0);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
       
        String extensions = gl.glGetString(GL10.GL_EXTENSIONS); 
        String version = gl.glGetString(GL10.GL_VERSION);
        String renderer = gl.glGetString(GL10.GL_RENDERER);
        boolean isSoftwareRenderer = renderer.contains("PixelFlinger");
        boolean isOpenGL10 = version.contains("1.0");
        boolean supportsDrawTexture = extensions.contains("draw_texture");
        boolean supportsVBOs = !isSoftwareRenderer && (!isOpenGL10 || extensions.contains("vertex_buffer_object"));
        
        DebugLog.i("GLGameRenderer", version + " (" + renderer + "): " +(supportsDrawTexture ?  "draw texture " : "") + (supportsVBOs ? "vbos" : ""));
	}

	/**
	 * This is only used in a CanvasGameRenderer. It is still present here to
	 * unify the different renderer interfaces.
	 */
	@Override
	public void onDrawFrame(Canvas canvas) {
		// Do nothing!
	}

	/**
	 * This is only used in a CanvasGameRenderer. It is still present here to
	 * unify the different renderer interfaces.
	 */
	@Override
	public void sizeChanged(int width, int height) {
		// Do nothing!
	}

	@Override
	public void clearCommands(DrawCommandPool pool) {
		while (!backQueue.empty()) {
			DrawCommand command = (DrawCommand) backQueue.dequeue();
			pool.release(command);
		}
	}

	@Override
	public void enqueue(DrawCommand command) {
		backQueue.enqueue(command);
	}

}
