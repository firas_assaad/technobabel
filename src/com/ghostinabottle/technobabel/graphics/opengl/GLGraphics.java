package com.ghostinabottle.technobabel.graphics.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.ghostinabottle.technobabel.graphics.Color;
import com.ghostinabottle.technobabel.graphics.GameRenderer;
import com.ghostinabottle.technobabel.graphics.Graphics;
import com.ghostinabottle.technobabel.graphics.Texture;

import android.graphics.Rect;

/**
 * A class for primitive OpenGL ES rendering.
 */
public class GLGraphics extends Graphics {
	/** OpenGL ES 1.0 handle. */
	GL10 gl;

	/**
	 * Set the GL handle
	 *
	 * @param gl
	 *            OpenGL ES handle used for drawing.
	 */
	public void setGl(GL10 gl) {
		this.gl = gl;
	}

	@Override
	public void clear() {
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	}

	@Override
	public void drawTexture(Texture texture, int x, int y) {
		drawTexture(texture, new Rect(x, y, x + texture.getWidth(), y + texture.getHeight()), null, -1);
	}

	@Override
	public void drawTexture(Texture texture, Rect srcRect, Rect destRect, int layer) {
		drawTexture(texture, srcRect, destRect, layer, -1);
	}

	@Override
	public void drawTexture(Texture texture, Rect srcRect, Rect destRect,
			int layer, int angle) {
		GLTexture glTexture = (GLTexture) texture;
		if (!glTexture.isLoaded())
			glTexture.load();
		glTexture.bind();
		gl.glPushMatrix();
		if (srcRect != null) {
			float scaleX = (float) destRect.width() / srcRect.width();
			float scaleY = (float) destRect.height() / srcRect.height();
			gl.glScalef(scaleX, scaleY, 1.0f);
		}
		if (angle != -1)
			gl.glRotatef(angle, 0, 0, 1.0f);
		layer = layer > -1 ?  layer - GameRenderer.MAX_NUMBER_OF_LAYERS + 1 : 0;
		float vertices[] = {
			destRect.left, destRect.top + srcRect.height(), layer,
			destRect.left, destRect.top, layer,
			destRect.left + srcRect.width(), destRect.top + srcRect.height(), layer,
			destRect.left + srcRect.width(), destRect.top, layer
		};
		int texWidth = glTexture.getTextureWidth();
		int texHeight = glTexture.getTextureHeight();
		float srcX = 0;
		float srcY = 0;
		float srcWidth = 1;
		float srcHeight = 1;
		if (srcRect != null) {
			srcX = (float) srcRect.left / texWidth;
			srcY = (float) srcRect.top / texHeight;
			srcWidth = (float) srcRect.right / texWidth;
			srcHeight = (float) srcRect.bottom / texHeight;
		}
		float tex[] = {
			srcX, srcHeight,
			srcX, srcY,
			srcWidth, srcHeight,
			srcWidth, srcY
		};
		
		ByteBuffer vByteBuffer = ByteBuffer.allocateDirect(4 * 3 * 4);
		vByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer vBuffer = vByteBuffer.asFloatBuffer();
		
		vBuffer.put(vertices);
		vBuffer.position(0);

        ByteBuffer tByteBuffer = ByteBuffer.allocateDirect(4 * 2 * 4);
        tByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer tBuffer = tByteBuffer.asFloatBuffer();
        
        tBuffer.put(tex);
        tBuffer.position(0);
		
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, tBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glPopMatrix();
	}

	@Override
	public void drawRect(Rect rect, int color) {
		float vertices[] = {
			rect.left, rect.top, 0,
			rect.left, rect.bottom, 0,
			rect.right, rect.bottom, 0,
			rect.right, rect.top, 0
		};
		
		ByteBuffer vByteBuffer = ByteBuffer.allocateDirect(4 * 3 * 4);
		vByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer vBuffer = vByteBuffer.asFloatBuffer();
		
		vBuffer.put(vertices);
		vBuffer.position(0);
		
		gl.glColor4f(Color.red(color), Color.green(color), Color.blue(color), Color.alpha(color));
		
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	}

}
