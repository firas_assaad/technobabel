package com.ghostinabottle.technobabel.graphics.opengl;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLU;
import android.opengl.GLUtils;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Texture;
import com.ghostinabottle.technobabel.util.Common;
import com.ghostinabottle.technobabel.util.DebugLog;

/**
 * Holds an OpenGL texture name.
 */
public class GLTexture extends Texture {
	/** Bitmap represented by the texture. */
	private Bitmap bitmap;
	/** GL instance. */
	private GL10 gl;
	/** The OpenGL texture ID. */
	private int textureID;
	/** Width of the texture (power of 2). */
	private int textureWidth;
	/** Height of the texture (power of 2). */
	private int textureHeight;
	/** Width of the image. */
	private int imageWidth;
	/** Height of the image. */
	private int imageHeight;
	/** Is the texture loaded? */
	private boolean loaded;
	/** Array for texture names. */
	private static int[] texArray = { 0 };
	/**
	 * Load the bitmap with the given resource ID.
	 * 
	 * @param game
	 *            Game object (for the resources).
	 * @param id
	 *            Bitmap resource ID.
	 */
	public GLTexture(Game game, GL10 gl, int resourceID) {
		textureID = 0;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		bitmap = BitmapFactory.decodeResource(
				game.getContext().getResources(),
				resourceID,
				options);
		imageWidth = bitmap.getWidth();
		imageHeight = bitmap.getHeight();
		textureWidth = Common.nextPowerOfTwo(imageWidth);
		textureHeight = Common.nextPowerOfTwo(imageHeight);
		this.gl = gl;
	}
	
	@Override
	public int getWidth() {
		return imageWidth;
	}
	
	@Override
	public int getHeight() {
		return imageHeight;
	}

	@Override
	public int getID() {
		return textureID;
	}
	
	/**
	 * 
	 * @return the power of two width of the texture
	 */
	public int getTextureWidth() {
		return textureWidth;
	}
	
	/**
	 * 
	 * @return the power of two height of the texture
	 */
	public int getTextureHeight() {
		return textureHeight;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void load() {
		gl.glGenTextures(1, texArray, 0);
		
		int error = gl.glGetError();
		if (error != GL10.GL_NO_ERROR) {
            DebugLog.e("GLTexture", "Error generating texture: " + error + " (" + GLU.gluErrorString(error) + ")");
        }
		
		textureID = texArray[0];

		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
		
		int format = GL10.GL_RGBA;
		int type = GL10.GL_UNSIGNED_BYTE;
		
        DebugLog.i("GLTexture", "Creating texture: " + error + " (" + GLU.gluErrorString(error) + ")" +
        		"format: " + format + ", util format: " + GLUtils.getInternalFormat(bitmap) + ", util type: " + 
        		GLUtils.getType(bitmap) + ", width: " + textureWidth + ", height: " + textureHeight + ", type: " + type);
		
        gl.glTexImage2D(GL10.GL_TEXTURE_2D, 0, format, textureWidth, textureHeight, 0, format, type, null);
		GLUtils.texSubImage2D(GL10.GL_TEXTURE_2D, 0, 0, 0, bitmap, format, type);
		
		error = gl.glGetError();
		if (error != GL10.GL_NO_ERROR) {
            DebugLog.e("GLTexture", "Error creating texture: " + error + " (" + GLU.gluErrorString(error) + ")" +
            		"format: " + format + ", width: " + textureWidth + ", height: " + textureHeight + ", type: " + type);
        }
		
		loaded = true;
	}
	
	public void bind() {
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textureID);
	}
}
