package com.ghostinabottle.technobabel.graphics.opengl;

import javax.microedition.khronos.opengles.GL10;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.Texture;
import com.ghostinabottle.technobabel.graphics.TextureFactory;

/**
 * Manages OpenGL texture creation and releasing.
 */
public class GLTextureFactory extends TextureFactory {
	/** GL ES 1.0 object. */
	private static GL10 gl;
	/** Used for releasing textures. */
	private static int[] texArray = { 0 };
	
	public GLTextureFactory(Game game) {
		super(game);
	}
	
	/** Set the GL10 instance. */
	public static void setGL(GL10 gl10) {
		gl = gl10;
	}
	
	@Override
	public Texture allocateTexture(int resourceId) {
		if (gl == null)
			return null;
		Texture texture = new GLTexture(game, gl, resourceId);
		return texture;
	}

	@Override
	public void releaseTexture(Texture texture) {
		if (gl == null)
			return;
		texArray[0] = texture.getID();
		gl.glDeleteTextures(1, texArray, 0);
	}

}
