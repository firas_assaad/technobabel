package com.ghostinabottle.technobabel.input;

import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * The input system. Classes can register themselves to receive updates.
 */
public class Input {
	/** Maximum number of registered handlers at a time. */
	private final static int NUMBER_OF_HANDLERS = 4;
	/** The low level input watcher. */
	private InputWatcher watcher;
	/** List of registered handlers for move events. */
	private FixedSizeArray<MoveEventHandler> moveHandlers;
	/** List of registered handlers for move events. */
	private FixedSizeArray<ShootEventHandler> shootHandlers;

	public Input() {
		watcher = new InputWatcher();
		moveHandlers = new FixedSizeArray<MoveEventHandler>(NUMBER_OF_HANDLERS);
		shootHandlers = new FixedSizeArray<ShootEventHandler>(
				NUMBER_OF_HANDLERS);
	}

	/**
	 * Reset input system by clearing the handlers.
	 */
	public void reset() {
		moveHandlers.clear();
		shootHandlers.clear();
	}

	/**
	 * Register the handler to receive move event updates.
	 * 
	 * @param handler
	 *            An object that handles move events.
	 */
	public void registerMovementEvent(MoveEventHandler handler) {
		moveHandlers.add(handler);
	}

	/**
	 * Unregister the handler so that it doesn't get any more updates.
	 * 
	 * @param handler
	 *            An object that handles move events.
	 */
	public void unregisterMovementEvent(MoveEventHandler handler) {
		moveHandlers.remove(handler, true);
	}

	/**
	 * Register the handler to receive shoot event updates.
	 * 
	 * @param handler
	 *            An object that handles shoot events.
	 */
	public void registerShootingEvent(ShootEventHandler handler) {
		shootHandlers.add(handler);
	}

	/**
	 * Unregister the handler so that it doesn't get any more updates.
	 * 
	 * @param handler
	 *            An object that handles shoot events.
	 */
	public void unregisterShootingEvent(ShootEventHandler handler) {
		shootHandlers.remove(handler, true);
	}

	/**
	 * 
	 * @return The input watcher.
	 */
	public InputWatcher getInputWatcher() {
		return watcher;
	}

	/**
	 * Check for pending events from the watcher and notify registered handlers.
	 */
	public void update() {
		if (watcher.hasMoveEvents()) {
			MoveEvent event = watcher.getMoveEvent();
			int handlersCount = moveHandlers.getCount();
			for (int i = 0; i < handlersCount; i++) {
				if (event.targetX >= 0 || event.targetY >= 0)
				{
					moveHandlers.get(i).moveTo(event.targetX, event.targetY);
				} else {
					moveHandlers.get(i).move(event.xVelocity, event.yVelocity);
				}
			}
			watcher.releaseMoveEvent(event);
		}
		if (watcher.hasShootEvents()) {
			ShootEvent event = watcher.getShootEvent();
			int handlersCount = shootHandlers.getCount();
			for (int i = 0; i < handlersCount; i++) {
				shootHandlers.get(i).shoot(event.isShooting);
			}
			watcher.releaseShootEvent(event);
		}

	}

	/**
	 * A move event handler handles movement events.
	 */
	public interface MoveEventHandler {
		/**
		 * Responds to move events.
		 * 
		 * @param xVelocity
		 *            Velocity in X direction.
		 * @param yVelocity
		 *            Velocity in Y direction.
		 */
		void move(float xVelocity, float yVelocity);
		
		/**
		 * Move to a specific location
		 * 
		 * @param targetX
		 *            Target location's X coordinate.
		 * @param targetY
		 *            Target location's Y coordinate.
		 */
		void moveTo(float targetX, float targetY);
	}

	/**
	 * A shoot event handler handles shooting events.
	 */
	public interface ShootEventHandler {
		/**
		 * Responds to shoot events.
		 * 
		 * @param isShooting
		 *            If true then the player is shooting.
		 */
		void shoot(boolean isShooting);
	}
}
