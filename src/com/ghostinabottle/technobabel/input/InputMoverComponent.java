package com.ghostinabottle.technobabel.input;

import com.ghostinabottle.technobabel.logic.GameComponent;
import com.ghostinabottle.technobabel.object.GameObject;

/**
 * A component for controlling an object. Receives input events and acts on the
 * object based on them.
 */
public class InputMoverComponent extends GameComponent implements
		Input.MoveEventHandler, Input.ShootEventHandler {
	
	private float moveTargetX;
	private float moveTargetY;

	/**
	 * Create the component and register it to handle input events
	 * 
	 * @param object
	 *            The game object holding this component.
	 */
	public InputMoverComponent(GameObject object) {
		super(object);
		moveTargetX = moveTargetY = -1.0f;
		object.getGame().getInput().registerMovementEvent(this);
		object.getGame().getInput().registerShootingEvent(this);
	}

	/**
	 * Update the component based on input.
	 */
	@Override
	public void update() {
		if (moveTargetX >= 0.0f)
			updateMoveTo();
		basicObject.x += basicObject.xVelocity;
		basicObject.y += basicObject.yVelocity;
	}

	/**
	 * Set object's velocity based on movement.
	 */
	@Override
	public void move(float xVelocity, float yVelocity) {
		moveTargetX = moveTargetY = -1.0f;
		// TODO: Use object's speed
		basicObject.xVelocity = xVelocity * 5;
		basicObject.yVelocity = yVelocity * 5;
	}
	
	/**
	 * Set object's velocity to move to a specific location.
	 */
	@Override
	public void moveTo(float targetX, float targetY) {
		moveTargetX= targetX;
		moveTargetY = targetY;
	}
	
	private void updateMoveTo() {
		basicObject.xVelocity = 0;
		basicObject.yVelocity = 0;
		// Move up and down
		float deltaY = basicObject.y + basicObject.height - moveTargetY;
		if (deltaY > 5) {
			basicObject.yVelocity = -5;
		} else if (deltaY < -5) {
			basicObject.yVelocity = 5;
		}
		// Move left and right
		float deltaX = basicObject.x - moveTargetX;
		if (deltaX > 5) {
			basicObject.xVelocity = -5;
		}
		if (deltaX < -5) {
			basicObject.xVelocity = 5;
		}
	}


	@Override
	public void dispose() {
		basicObject.getGame().getInput().unregisterMovementEvent(this);
	}

	@Override
	public void shoot(boolean isShooting) {
		((GameObject) basicObject).isShooting = isShooting;
	}

}
