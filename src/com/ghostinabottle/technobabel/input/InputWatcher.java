package com.ghostinabottle.technobabel.input;

import com.ghostinabottle.technobabel.object.ObjectPool;
import com.ghostinabottle.technobabel.util.ArrayQueue;
import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * Receives raw input events and translates them to a meaningful game format
 */
public class InputWatcher {

	/** Maximum number of events at a time. */
	private static final int NUMBER_OF_EVENTS = 100;
	/** Pool of move events used by the queue. */
	private MoveEventPool moveEventsPool;
	/** Queue of movement events. */
	private ArrayQueue<MoveEvent> moveEvents;
	/** Pool of shoot events used by the queue. */
	private ShootEventPool shootEventsPool;
	/** Queue of shoot events. */
	private ArrayQueue<ShootEvent> shootEvents;
	/** Last x coordinate for touch events. */
	private float touchLastX;
	/** Last y coordinate for touch events. */
	private float touchLastY;
	/** Needed change between touch events. */
	private static final float touchEventsChange = 1.0f;

	public InputWatcher() {
		moveEventsPool = new MoveEventPool(NUMBER_OF_EVENTS);
		moveEvents = new ArrayQueue<MoveEvent>(NUMBER_OF_EVENTS);
		shootEventsPool = new ShootEventPool(NUMBER_OF_EVENTS);
		shootEvents = new ArrayQueue<ShootEvent>(NUMBER_OF_EVENTS);
		touchLastX = -1.0f;
		touchLastY = -1.0f;
	}

	/**
	 * Get the oldest move event on the queue.
	 *
	 * @return First move event on the queue.
	 */
	public MoveEvent getMoveEvent() {
		return moveEvents.dequeue();
	}

	/**
	 * Release the event to be reused.
	 *
	 * @param event
	 *            Event to release.
	 */
	public void releaseMoveEvent(MoveEvent event) {
		moveEventsPool.release(event);
	}

	/**
	 *
	 * @return True if there are pending move events.
	 */
	public boolean hasMoveEvents() {
		return !moveEvents.empty();
	}

	/**
	 * Get the oldest move event on the queue.
	 *
	 * @return First move event on the queue.
	 */
	public ShootEvent getShootEvent() {
		return shootEvents.dequeue();
	}

	/**
	 * Release the event to be reused.
	 *
	 * @param event
	 *            Event to release.
	 */
	public void releaseShootEvent(ShootEvent event) {
		shootEventsPool.release(event);
	}

	/**
	 *
	 * @return True if there are pending shoot events.
	 */
	public boolean hasShootEvents() {
		return !shootEvents.empty();
	}

	/**
	 * Called when a key is pressed.
	 *
	 * @param keyCode
	 *            Code of the key that was pressed.
	 * @param event
	 *            Event information.
	 * @return True if event was handler.
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		MoveEvent moveEvent = null;
		ShootEvent shootEvent = null;
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_UP:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = 0;
			moveEvent.yVelocity = -1;
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = 0;
			moveEvent.yVelocity = 1;
			break;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = -1;
			moveEvent.yVelocity = 0;
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = 1;
			moveEvent.yVelocity = 0;
			break;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			shootEvent = (ShootEvent) shootEventsPool.allocate();
			shootEvent.isShooting = true;
			break;
		}
		if (moveEvent != null)
			moveEvents.enqueue(moveEvent);
		if (shootEvent != null)
			shootEvents.enqueue(shootEvent);
		return true;
	}

	/**
	 * Called when a key is released.
	 *
	 * @param keyCode
	 *            Code of the key that was released.
	 * @param event
	 *            Event information.
	 * @return True if event was handler.
	 */
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		MoveEvent moveEvent = null;
		ShootEvent shootEvent = null;
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_UP:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = moveEvent.yVelocity = 0;
			moveEvent.targetX = moveEvent.targetY = -1;
			break;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = moveEvent.yVelocity = 0;
			moveEvent.targetX = moveEvent.targetY = -1;
			break;
		case KeyEvent.KEYCODE_DPAD_LEFT:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = moveEvent.yVelocity = 0;
			moveEvent.targetX = moveEvent.targetY = -1;
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = moveEvent.yVelocity = 0;
			moveEvent.targetX = moveEvent.targetY = -1;
			break;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			shootEvent = (ShootEvent) shootEventsPool.allocate();
			shootEvent.isShooting = false;
			break;
		}
		if (moveEvent != null)
			moveEvents.enqueue(moveEvent);
		if (shootEvent != null)
			shootEvents.enqueue(shootEvent);
		return true;
	}
	
	public boolean onTouchEvent(MotionEvent event) {
		ShootEvent shootEvent = null;
		MoveEvent moveEvent = null;
		if (event == null) {
			return false;
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			moveEvent = (MoveEvent) moveEventsPool.allocate();
			moveEvent.xVelocity = moveEvent.yVelocity = 0;
			moveEvent.targetX = moveEvent.targetY = -1;
			shootEvent = (ShootEvent) shootEventsPool.allocate();
			shootEvent.isShooting = false;
		}
		else if (event.getAction() == MotionEvent.ACTION_DOWN ||
				event.getAction() == MotionEvent.ACTION_MOVE) {
			// Skip small updates
			if (Math.abs(event.getX() - touchLastX) >= touchEventsChange ||
					Math.abs(event.getY() - touchLastY) >= touchEventsChange) {
				touchLastX = event.getX();
				touchLastY = event.getY();
				moveEvent = (MoveEvent) moveEventsPool.allocate();
				moveEvent.targetX = event.getX();
				moveEvent.targetY = event.getY();
				shootEvent = (ShootEvent) shootEventsPool.allocate();
				shootEvent.isShooting = true;
			}
		}
		if (moveEvent != null)
			moveEvents.enqueue(moveEvent);
		if (shootEvent != null)
			shootEvents.enqueue(shootEvent);

		return true;
	}

	/**
	 * Pool of move events.
	 */
	class MoveEventPool extends ObjectPool {

		public MoveEventPool(int size) {
			super(size);
		}
		
		@Override
		public void release(Object entry) {
			MoveEvent event = (MoveEvent) entry;
			event.targetX = event.targetY = -1.0f;
			event.xVelocity = event.yVelocity = 0.0f;
			super.release(entry);
		}

		@Override
		protected void fill() {
			int size = getSize();
			for (int i = 0; i < size; i++) {
				getAvailable().add(new MoveEvent());
			}
		}

	}

	/**
	 * Pool of shoot events.
	 */
	class ShootEventPool extends ObjectPool {

		public ShootEventPool(int size) {
			super(size);
		}
		
		@Override
		public void release(Object entry) {
			ShootEvent event = (ShootEvent) entry;
			event.isShooting = false;
			super.release(entry);
		}

		@Override
		protected void fill() {
			int size = getSize();
			for (int i = 0; i < size; i++) {
				getAvailable().add(new ShootEvent());
			}
		}

	}
}
