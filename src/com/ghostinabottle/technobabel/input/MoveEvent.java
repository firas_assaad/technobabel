package com.ghostinabottle.technobabel.input;

/**
 * Information about a movement input event.
 */
public class MoveEvent {
	/** Horizontal velocity. */
	public float xVelocity;
	/** Vertical velocity. */
	public float yVelocity;
	/** Movement target position's X coordinate */
	public float targetX = -1.0f;
	/** Movement target position's Y coordinate */
	public float targetY = -1.0f;
}
