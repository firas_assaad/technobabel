package com.ghostinabottle.technobabel.input;

/**
 * Information about user pressing action key to shoot.
 */
public class ShootEvent {
	/** Is the player shooting or not? */
	public boolean isShooting;
}
