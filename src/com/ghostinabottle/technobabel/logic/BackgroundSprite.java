package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.graphics.SpriteRenderer;

/** A background sprite is a simple sprite handle with optional delay after it. */
public final class BackgroundSprite {
	/** Is it a normal sprite (default) or a background? */
	public boolean isBackground;
	/** Sprite data object. */
	public SpriteRenderer renderer;
	/** Starting position. */
	public int startX, startY;
	/** Current screen position. */
	public int x, y;
	/** Layer where the sprite is. */
	public int layer;
	/** Velocity in pixels. */
	public int xVelocity, yVelocity;
}
