package com.ghostinabottle.technobabel.logic;

import android.graphics.Rect;

/**
 * An axially aligned bounding box (AABB).
 */
public final class BoundingBox {
	/** Rectangle representing left, top, right, and bottom end points. */
	private Rect rect;

	/**
	 * Create a bounding box with (0, 0, 0, 0) as the points.
	 */
	public BoundingBox() {
		rect = new Rect();
	}

	/**
	 * Create bounding box with supplied end points.
	 *
	 * @param left
	 *            Top left point x.
	 * @param top
	 *            Top left point y.
	 * @param right
	 *            Bottom right point x.
	 * @param bottom
	 *            Bottom right point y.
	 */
	public BoundingBox(int left, int top, int right, int bottom) {
		rect = new Rect(left, top, right, bottom);
	}

	/**
	 * Set the end points for the bounding box.
	 *
	 * @param left
	 *            Top left point x.
	 * @param top
	 *            Top left point y.
	 * @param right
	 *            Bottom right point x.
	 * @param bottom
	 *            Bottom right point y.
	 */
	public void set(int left, int top, int right, int bottom) {
		rect.set(left, top, right, bottom);
	}

	/**
	 * Check if the bounding box intersects another.
	 *
	 * @param other
	 *            Other bounding box to check.
	 * @return True if there is an intersection.
	 */
	public boolean intersects(BoundingBox other) {
		Rect otherRect = other.getRect();
		return  rect.left <= otherRect.right && otherRect.left <= rect.right
		        && rect.top <= otherRect.bottom && otherRect.top <= rect.bottom;
	}

	/**
	 *
	 * @return X coordinate of top left point.
	 */
	public int getMinX() {
		return rect.left;
	}

	/**
	 *
	 * @return X coordinate of bottom right point.
	 */
	public int getMaxX() {
		return rect.right;
	}

	/**
	 *
	 * @return Y coordinate of top left point.
	 */
	public int getMinY() {
		return rect.top;
	}

	/**
	 *
	 * @return Y coordinate of bottom right point.
	 */
	public int getMaxY() {
		return rect.bottom;
	}

	/**
	 *
	 * @return Width of bounding box.
	 */
	public int getWidth() {
		return rect.width();
	}

	/**
	 *
	 * @return Height of bounding box.
	 */
	public int getHeight() {
		return rect.height();
	}

	/**
	 *
	 * @return Rectangle holding the points.
	 */
	public Rect getRect() {
		return rect;
	}

}
