package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.BasicObject;
import com.ghostinabottle.technobabel.object.BulletObject;
import com.ghostinabottle.technobabel.object.GameObject;
import com.ghostinabottle.technobabel.object.GameObjectFactory;

/**
 * A component that emits bullets.
 */
public class BulletEmitterComponent extends EmitterComponent {

	public BulletEmitterComponent(GameObject object, int delay) {
		super(object, delay);
	}

	@Override
	public BasicObject allocate() {
		// TODO : make it work for enemies too (based on direction)
		BulletObject object = GameObjectFactory.createBullet("player bullet");
		return object;
	}

	@Override
	public void update() {
		if (((GameObject) basicObject).isShooting)
			super.update();
	}

}
