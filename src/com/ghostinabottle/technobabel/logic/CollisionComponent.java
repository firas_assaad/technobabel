package com.ghostinabottle.technobabel.logic;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.logic.CollisionDetector.EndPoint;
import com.ghostinabottle.technobabel.logic.CollisionDetector.EndPointType;
import com.ghostinabottle.technobabel.object.BasicObject;
import com.ghostinabottle.technobabel.object.ObjectType;

/**
 * A component that keeps track of collisions with other objects.
 */
public class CollisionComponent extends GameComponent {
	/** Bounding box for the object that owns this component. */
	private BoundingBox boundingBox;
	/** Is the component registered with the collision detection system? */
	private boolean registered;
	/** Was the component disposed by the object owning it? */
	private boolean disposed;
	/** Component index in the collision detector. */
	private int id;
	/** X position in previous frame. */
	private float lastX;
	/** Y position in previous frame. */
	private float lastY;
	/** End points. */
	private EndPoint[] endPoints;
	/** The other component being collided with. */
	private CollisionComponent collider;

	public CollisionComponent() {
		super(null);
		int maxVal = Integer.MAX_VALUE - 1;
		boundingBox = new BoundingBox(maxVal, maxVal, maxVal, maxVal);
		endPoints = new EndPoint[4];
		endPoints[0] = new EndPoint(this, EndPointType.START);
		endPoints[1] = new EndPoint(this, EndPointType.END);
		endPoints[2] = new EndPoint(this, EndPointType.START);
		endPoints[3] = new EndPoint(this, EndPointType.END);
	}

	/**
	 * Register component with the collision detection system.
	 *
	 * @param object
	 *            Attached game object.
	 */
	public void set(BasicObject object) {
		this.basicObject = object;
		lastX = -1.0f;
		lastY = -1.0f;
		int maxVal = Integer.MAX_VALUE - 1;
		boundingBox.set(maxVal, maxVal, maxVal, maxVal);
		for (EndPoint e : endPoints) {
			e.disposed = false;
		}
		this.registered = true;
		this.disposed = false;
	}

	/**
	 *
	 * @return True if component is registered with the collision detector.
	 */
	public boolean isRegistered() {
		return registered;
	}

	/**
	 * Update the bounding box.
	 */
	@Override
	public void update() {
		setBoundingBox();
		if (basicObject.isExploding || basicObject.isDead) {
			unregister();
		}
	}

	/**
	 * Called when a collision is detected with the component 'other'.
	 * @param other
	 */
	public void startCollision(CollisionComponent other) {
		// TODO : use attack strength
		if (collider == null)
			basicObject.health -= 1;
		collider = other;
	}

	/**
	 * Called when a collision is ended
	 */
	public void endCollision() {
		collider = null;
	}

	/**
	 * Unregister this component from the collision detection system.
	 */
	public void unregister() {
		registered = false;
		if (collider != null) {
			collider.endCollision();
			endCollision();
		}
	}

	/**
	 * Unregister with the collision detection system.
	 */
	@Override
	public void dispose() {
		unregister();
		lastX = -1.0f;
		lastY = -1.0f;
		disposed = true;
	}

	/**
	 *
	 * @return The other component colliding with this one
	 */
	public CollisionComponent getCollider() {
		return collider;
	}

	/**
	 *
	 * @return True if the component was disposed
	 */
	public boolean isDisposed() {
		return disposed;
	}

	/**
	 *
	 * @return The bounding box.
	 */
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	/**
	 *
	 * @param id
	 *            Index in collision detection system.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return Index in collision detection system.
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @return Object's type.
	 */
	public ObjectType getType() {
		return basicObject.type;
	}

	/**
	 *
	 * @return The associated object.
	 */
	public BasicObject getObject() {
		return basicObject;
	}

	/**
	 * Set bounding box based on the object's position.
	 */
	private void setBoundingBox() {
		if (!basicObject.isActive)
			return;
		// Coordinates of sprite bounding box
		int minX, minY, width, height;
		minX = minY = 0;
		width = basicObject.width;
		height = basicObject.height;
		if (basicObject.boundingBox != null) {
			Rect rect = basicObject.boundingBox.getRect();
			if (rect.left > 0)
				minX = rect.left;
			if (rect.top > 0)
				minY = rect.top;
			if (rect.right > 0)
				width = rect.width();
			if (rect.bottom > 0)
				height = rect.height();
		}
		// Box X and Y from last frame
		if (lastX < 0)
			lastX = basicObject.x + minX;
		if (lastY < 0)
			lastY = basicObject.y + minY;
		// Box X and Y from this frame
		float newX = basicObject.x + minX;
		float newY = basicObject.y + minY;
		// Final bounding box coordinates
		float boxX, boxY, boxWidth, boxHeight;
		boxX = boxY = boxWidth = boxHeight = 0.0f;
		if (newX > lastX) {
			// Object is moving to the right
			boxX = lastX;
			boxWidth = (newX - lastX) + width;
		} else {
			// Object is moving to the left
			boxX = newX;
			boxWidth = (lastX - newX) + width;
		}
		if (newY > lastY) {
			// Object is moving downwards
			boxY = lastY;
			boxHeight = (newY - lastY) + height;
		} else {
			// Object is moving upwards
			boxY = newY;
			boxHeight = (lastY - newY) + height;
		}
		boundingBox.set((int) boxX, (int) boxY,
				(int) (boxX + boxWidth), (int) (boxY + boxHeight));
		lastX = newX;
		lastY = newY;
	}

	/**
	 *
	 * @return Array of end points.
	 */
	public EndPoint[] getEndPoints() {
		return endPoints;
	}

	/**
	 *
	 * @return True if all of the components end points were removed.
	 */
	public boolean endPointsDisposed() {
		if (registered)
			return false;
		for (EndPoint e : endPoints) {
			if (!e.disposed)
				return false;
		}
		return true;
	}

}
