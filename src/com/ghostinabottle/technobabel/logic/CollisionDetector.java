/**
 *
 */
package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.object.BasicObject;
import com.ghostinabottle.technobabel.object.ObjectPool;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * A system for detecting collisions using bounding boxes and the sweep and
 * prune algorithm. Components register themselves with the system to receive
 * collision detection notification.
 * The system is based on this article: http://www.shmup-dev.com/forum/index.php?topic=1635.0
 */
public final class CollisionDetector {
	/** List of registered collision components. */
	private FixedSizeArray<CollisionComponent> collisionComponents;
	/** List of horizontal end points. */
	private FixedSizeArray<EndPoint> xEndPoints;
	/** List of vertical end points. */
	private FixedSizeArray<EndPoint> yEndPoints;
	/** A reusable pool of collision components. */
	private static CollisionComponentPool componentsPool;

	public CollisionDetector() {
		int max = Game.MAX_NUMBER_OF_OBJECTS;
		collisionComponents = new FixedSizeArray<CollisionComponent>(max);
		xEndPoints = new FixedSizeArray<EndPoint>(2 * max);
		yEndPoints = new FixedSizeArray<EndPoint>(2 * max);
		componentsPool = new CollisionComponentPool(2 * max);
	}

	/**
	 * Reset the collision detection system.
	 */
	public void reset() {
		int max = Game.MAX_NUMBER_OF_OBJECTS;
		collisionComponents = new FixedSizeArray<CollisionComponent>(max);
		xEndPoints = new FixedSizeArray<EndPoint>(2 * max);
		yEndPoints = new FixedSizeArray<EndPoint>(2 * max);
		componentsPool = new CollisionComponentPool(2 * max);
	}

	/**
	 * Allocate and return a collision component.
	 */
	public CollisionComponent createCollisionComponent(BasicObject object) {
		CollisionComponent component = (CollisionComponent) componentsPool
				.allocate();
		component.set(object);
		registerCollisionComponent(component);
		return component;
	}

	/**
	 * Register a collision component to be notified on collisions.
	 *
	 * @param component
	 *            A collision component.
	 * @return Index of the component in the array of collision components.
	 */
	public void registerCollisionComponent(CollisionComponent component) {
		cleanup();
		collisionComponents.add(component);
		// Move unregistered components to the end of the list
		int id = collisionComponents.getCount() - 1;
		CollisionComponent prevComponent = null;
		while (id > 0) {
			int prevId = id - 1;
			prevComponent = collisionComponents.get(prevId);
			if (!prevComponent.isRegistered()) {
				prevComponent.setId(id);
				collisionComponents.set(id, prevComponent);
				collisionComponents.set(prevId, component);
				id--;
			} else {
				break;
			}
		}
		EndPoint[] endPoints = component.getEndPoints();
		xEndPoints.add(endPoints[0]);
		xEndPoints.add(endPoints[1]);
		yEndPoints.add(endPoints[2]);
		yEndPoints.add(endPoints[3]);
		component.setId(id);
	}

	/**
	 * Get an end point value based on type and axis.
	 *
	 * @param box
	 *            Bounding box to get the value from.
	 * @param type
	 *            Type of end point (start or end).
	 * @param axis
	 *            Which axis? (X or Y)
	 * @return
	 */
	public static int getValue(BoundingBox box, EndPointType type, AxisType axis) {
		// If this is a start point get the minimum value (minX or minY),
		// otherwise get the maximum value (maxX or maxY)
		int value;
		if (axis == AxisType.X_AXIS) {
			if (type == EndPointType.START) {
				value = box.getMinX();
			} else {
				value = box.getMaxX();
			}
		} else {
			if (type == EndPointType.END) {
				value = box.getMinY();
			} else {
				value = box.getMaxY();
			}
		}
		return value;
	}

	/**
	 * Check for collisions along a single axis.
	 *
	 * @param endPoints
	 *            End points for this axis.
	 * @param axis
	 *            Axis type (vertical or horizontal.
	 */
	public static void updateAxis(FixedSizeArray<EndPoint> endPoints,
			AxisType axis) {
		int endPointCount = endPoints.getCount();
		for (int j = 0; j < endPointCount; j++) {
			EndPoint point = endPoints.get(j);
			EndPointType type = point.type;
			CollisionComponent component = point.component;
			// Skip unregistered points
			if (!component.isRegistered()) {
				continue;
			}

			BoundingBox box = component.getBoundingBox();

			int value = getValue(box, type, axis);


			if (value == Integer.MAX_VALUE - 1) {
				continue;
			}

			// Previous value index
			int i = j - 1;

			// Compare the value to values before it swapping places if needed
			while (i >= 0) {
				EndPoint prevPoint = endPoints.get(i);
				EndPointType prevType = prevPoint.type;
				CollisionComponent prevComponent = prevPoint.component;
				BoundingBox prevBox = prevComponent.getBoundingBox();
				boolean prevRegistered = prevComponent.isRegistered();

				// If the previous end point has an unregistered component,
				// use a large value to push the end point to the end of
				// the array for cleanup.
				int prevValue = prevRegistered ? getValue(prevBox, prevType,
						axis) : Integer.MAX_VALUE;

				if (prevValue < value) {
					// The order is correct, exit while loop
					break;
				}

				// If we get here then we need to do some swapping

				// If the objects have the same type or one is unregistered then
				// there's
				// no real collision
				if (prevComponent.getType() == component.getType()
						|| !prevComponent.isRegistered()
						|| !component.isRegistered()) {
					// Do nothing~
				} else if (prevType == EndPointType.START
						&& type == EndPointType.END &&
						component.getCollider() != null &&
						prevComponent.getCollider() != null) {
					if (component.getId() == prevComponent.getCollider().getId() &&
							prevComponent.getId() == component.getCollider().getId()) {
						// If the current point is an end point and there's a start
						// point to the left then object is leaving collision
						component.endCollision();
						prevComponent.endCollision();
					}
				} else {
					// If the current point is a start point and there's an end
					// point to the left then the objects might be colliding
					if (prevType == EndPointType.END
							&& type == EndPointType.START) {
						// This only means they overlap in one axis, we use
						// a more accurate test...
						if (box.intersects(prevBox)) {
							component.startCollision(prevComponent);
							prevComponent.startCollision(component);
						}
					}
				}

				// Finally we swap the current point with the one before it
				endPoints.set(i + 1, prevPoint);
				endPoints.set(i, point);

				// Continue swapping down the array
				i--;
			}

		}
	}

	/**
	 * Called every frame to check for collisions.
	 */
	public void update() {
		updateAxis(xEndPoints, AxisType.X_AXIS);
		// TODO : Do we need to check the Y axis?
		updateAxis(yEndPoints, AxisType.Y_AXIS);
		cleanup();
	}

	/**
	 * Get rid of end points belonging to unregistered components,
	 * and delete any disposed components.
	 */
	public void cleanup() {
		// We only need to check at the end of the lists because end points
		// with unregistered components are pushed to the end in update.
		CollisionComponent component;
		int i = xEndPoints.getCount() - 1;
		while (i >= 0) {
			EndPoint endPoint = xEndPoints.get(i);
			component = endPoint.component;
			if (component.isRegistered()) {
				if (component.getObject().isActive) {
					break;
				}
			} else {
				endPoint.disposed = true;
				attemptToReleaseComponent(component);
				xEndPoints.swapWithLast(i);
				xEndPoints.removeLast();
			} 
			i--;
		}
		i = yEndPoints.getCount() - 1;
		while (i >= 0) {
			EndPoint endPoint = yEndPoints.get(i);
			component = endPoint.component;
			if (component.isRegistered()) {
				if (component.getObject().isActive) {
					break;
				}
			} else {
				endPoint.disposed = true;
				attemptToReleaseComponent(component);
				yEndPoints.swapWithLast(i);
				yEndPoints.removeLast();
			} 
			i--;
		}
		i = collisionComponents.getCount() - 1;
		while (i >= 0) {
			component = collisionComponents.get(i);
			if (component.isDisposed()) {
				// Actually release the component
				collisionComponents.removeLast();
				componentsPool.release(component);
				i--;
			} else {
				break;
			}
		}
	}

	/**
	 * Mark collision components with all end points disposed for
	 * deletion by moving them to the end of the components list
	 *
	 * @param component
	 */
	private void attemptToReleaseComponent(CollisionComponent component) {
		if (!component.endPointsDisposed())
			return;
		if (collisionComponents.empty())
			return;
		int id = component.getId();
		int lastId = collisionComponents.getCount() - 1;
		CollisionComponent lastComponent = null;
		while (lastId >= 0) {
			lastComponent = collisionComponents.get(lastId);
			if (lastComponent.isDisposed()) {
				lastId--;
			} else {
				break;
			}
		}
		if (lastId < 0)
			lastId = 0;

		// Move component to the end of the list to be checked for actual deletion later
		lastComponent.setId(id);
		component.setId(lastId);
		collisionComponents.set(lastId, component);
		collisionComponents.set(id, lastComponent);
	}

	/** Types of end point axis (x, y). */
	private enum AxisType {
		X_AXIS, Y_AXIS
	}

	/** Types of end points (top left, bottom right). */
	public static enum EndPointType {
		START, END
	}

	/** A bounding box end point. */
	public static class EndPoint {
		/** Collision component associated with this point. */
		public CollisionComponent component;
		/** Type of end point. */
		public EndPointType type;
		/** Was end point disposed? */
		public boolean disposed;

		public EndPoint(CollisionComponent component, EndPointType type) {
			this.component = component;
			this.type = type;
			this.disposed = false;
		}
	}

	/**
	 * A pool of collision components.
	 *
	 */
	public class CollisionComponentPool extends ObjectPool {

		public CollisionComponentPool(int size) {
			super(size);
		}

		/**
		 * Fill the pool with collision components.
		 */
		@Override
		protected void fill() {
			int size = getSize();
			for (int i = 0; i < size; i++) {
				getAvailable().add(new CollisionComponent());
			}
		}

	}
}
