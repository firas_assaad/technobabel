package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.BasicObject;

/** Abstract class for computer controlled movers. */
public abstract class ComputerMoverComponent extends GameComponent {
	/** Facing direction. */
	protected FacingDirection face;
	// TODO : Use standard speed measure
	/** Movement speed. */
	protected float speed;

	/**
	 * 
	 * @param object
	 *            BasicObject that holds this component.
	 * @param face
	 *            Facing direction (FACE_LEFT or FACE_RIGHT).
	 * @param speed
	 *            Movement speed.
	 */
	public ComputerMoverComponent(BasicObject object, FacingDirection face,
			float speed) {
		super(object);
		this.face = face;
		this.speed = speed;
	}

	/**
	 * 
	 * @param object
	 *            BasicObject that holds this component.
	 * @param face
	 *            Facing direction (FACE_LEFT or FACE_RIGHT).
	 * @param speed
	 *            Movement speed.
	 */
	public void set(BasicObject object, FacingDirection face, float speed) {
		this.basicObject = object;
		this.face = face;
		this.speed = speed;
	}

	@Override
	public abstract void update();

}
