package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.BasicObject;
import com.ghostinabottle.technobabel.object.GameObject;
import android.os.SystemClock;

/** A component that creates objects of type T */
public abstract class EmitterComponent extends GameComponent {
	/** The time in milliseconds of the last emit. */
	private long lastEmit;
	/** Number of milliseconds between each emit.. */
	private int delay;
	/** Offset X position of omitted objects from the emitter's game object. */
	private int xOffset;
	/** Offset Y position of omitted objects from the emitter's game object. */
	private int yOffset;

	/**
	 * 
	 * @param object
	 *            Object that owns this component.
	 * @param delay
	 *            Number of milliseconds between each emit.
	 */
	public EmitterComponent(GameObject object, int delay) {
		super(object);
		this.delay = delay;
		this.lastEmit = SystemClock.uptimeMillis();
	}

	/**
	 * Emit an object every delay milliseconds.
	 */
	@Override
	public void update() {
		if (SystemClock.uptimeMillis() - lastEmit < delay) {
			return;
		}
		BasicObject object = allocate();
		object.x = basicObject.x + xOffset;
		object.y = basicObject.y + yOffset;
		this.lastEmit = SystemClock.uptimeMillis();
	}

	/**
	 * Offset position of omitted objects from the emitter's game object.
	 * 
	 * @param x
	 *            X offset.
	 * @param y
	 *            Y offset.
	 */
	public void setOffsets(int x, int y) {
		xOffset = x;
		yOffset = y;
	}

	/**
	 * Override this to create the proper object and return it.
	 * 
	 * @return A BasicObject constructed using the object factory.
	 */
	public abstract BasicObject allocate();

}
