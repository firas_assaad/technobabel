package com.ghostinabottle.technobabel.logic;

/**
 * An object's facing direction.
 */
public enum FacingDirection {
	FACE_LEFT, FACE_RIGHT
}
