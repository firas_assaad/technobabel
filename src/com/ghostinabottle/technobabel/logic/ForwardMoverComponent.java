package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.BasicObject;

/**
 * A simple component that moves an object forward
 */
public class ForwardMoverComponent extends ComputerMoverComponent {

	/**
	 *
	 * @param object
	 *            BasicObject that holds this component.
	 * @param face
	 *            Facing direction (FACE_RIGHT or FACE_LEFT).
	 */
	public ForwardMoverComponent(BasicObject object, FacingDirection face,
			float speed) {
		super(object, face, speed);
	}

	/**
	 * Update the component to move forward.
	 */
	@Override
	public void update() {
		if (basicObject.isExploding) {
			basicObject.xVelocity = basicObject.yVelocity = 0.0f;
			return;
		}
		if (face == FacingDirection.FACE_RIGHT) {
			basicObject.xVelocity = speed;
		} else {
			basicObject.xVelocity = -speed;
		}
		basicObject.yVelocity = 0.0f;
		basicObject.x += basicObject.xVelocity;
		basicObject.y += basicObject.yVelocity;
	}

}
