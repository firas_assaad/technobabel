package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.BasicObject;

/**
 * A game component. Abstract class for any game component. Allows updating
 * based on frame time delta.
 */
public abstract class GameComponent {
	/** The game object that has this component. */
	protected BasicObject basicObject;

	public GameComponent(BasicObject object) {
		this.basicObject = object;
	}

	/**
	 * Called when the game object is cleared. Override if you want component to
	 * do something before being removed.
	 */
	public void dispose() {
	}

	/**
	 * Called on each component in the game object to update.
	 */
	public abstract void update();
}
