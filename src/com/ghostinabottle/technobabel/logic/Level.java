package com.ghostinabottle.technobabel.logic;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.SAXException;

import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.util.Xml;
import android.util.Xml.Encoding;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.R;
import com.ghostinabottle.technobabel.graphics.DisplaySize;
import com.ghostinabottle.technobabel.graphics.LevelRenderer;
import com.ghostinabottle.technobabel.object.GameObjectFactory;
import com.ghostinabottle.technobabel.util.ArrayQueue;
import com.ghostinabottle.technobabel.util.BackgroundSaxHandler;
import com.ghostinabottle.technobabel.util.DebugLog;
import com.ghostinabottle.technobabel.util.FixedSizeArray;
import com.ghostinabottle.technobabel.util.GameTime;
import com.ghostinabottle.technobabel.util.LevelSaxHandler;

/**
 *
 * A class that loads and processes game levels.
 */
public class Level {
	/** Maximum number of scenes in a level. */
	private static final int MAXIMUM_SCENES = 10;
	/** Maximum numbers of events to be processed in a level. */
	private static final int MAXIMUM_GAME_EVENTS = 256;
	/** Game instance. */
	private Game game;
	/** Level background renderer. */
	private LevelRenderer renderer;
	/** Resources object. */
	private Resources resources;
	/** Queue of background scenes. */
	private ArrayQueue<Scene> scenes;
	/** Current scene. */
	private Scene currentScene;
	/** List of level events. */
	private FixedSizeArray<LevelEvent> levelEvents;
	/** The event being processed. */
	private int currentEventIndex;
	/** Level SAX handler. */
	private LevelSaxHandler levelHandler;
	/** Background SAX Handler. */
	private BackgroundSaxHandler backgroundHandler;
	/** Is the level complete? */
	private boolean levelCompleted;

	public Level(Game game, Resources resources) {
		this.game = game;
		renderer = new LevelRenderer(this);
		this.resources = resources;
		scenes = new ArrayQueue<Scene>(MAXIMUM_SCENES);
		levelEvents = new FixedSizeArray<LevelEvent>(MAXIMUM_GAME_EVENTS);
		currentEventIndex = 0;
		levelHandler = new LevelSaxHandler(this);
		backgroundHandler = new BackgroundSaxHandler(this);
	}

	/**
	 * Reset the level.
	 */
	public void reset() {
		levelEvents.clear();
		scenes.clear();
		currentEventIndex = 0;
		levelCompleted = false;
	}

	/**
	 * Load a level from an XML file.
	 *
	 * @param fileId
	 *            ID of file to load
	 */
	public void loadLevel(int fileId) {
		InputStream istream = resources.openRawResource(fileId);
		try {
			Xml.parse(istream, Encoding.UTF_8, levelHandler);
		} catch (NotFoundException e) {
			DebugLog.e("Level", "Level XML file " + fileId + " not found.");
		} catch (IOException e) {
			DebugLog.e("Level", "IO Exception when opening level XML file "
					+ fileId + " - " + e.getMessage());
		} catch (SAXException e) {
			DebugLog.e("Level", "SAX Exception when opening level XML file "
					+ fileId + " - " + e.getMessage());
		}
		// TODO: TESTING
		loadBackground(R.raw.scocean);
	}

	public void loadBackground(int fileId) {
		InputStream istream = resources.openRawResource(fileId);
		try {
			Xml.parse(istream, Encoding.UTF_8, backgroundHandler);
		} catch (NotFoundException e) {
			DebugLog.e("Level", "Level XML file " + fileId + " not found.");
		} catch (IOException e) {
			DebugLog.e("Level", "IO Exception when opening level XML file "
					+ fileId + " - " + e.getMessage());
		} catch (SAXException e) {
			DebugLog.e("Level", "SAX Exception when opening level XML file "
					+ fileId + " - " + e.getMessage());
		}
	}

	/**
	 * Add an event to the list of events.
	 *
	 * @param event
	 */
	public void addGameEvent(LevelEvent event) {
		levelEvents.add(event);
	}

	/**
	 * Add a scene to the level's scenes
	 *
	 * @param scene
	 */
	public void addScene(Scene scene) {
		scenes.enqueue(scene);
	}

	/**
	 * Process an event. Sets the done property of the event to true if the
	 * event finished processing. Also recursively processes the children.
	 *
	 * @param event
	 */
	public void processEvent(LevelEvent event) {
		if (event.done) {
			return;
		}
		event.done = true;
		if (event.delaying) {
			if (GameTime.passed(event.delayStartTime, event.delay)) {
				event.delay = 0;
				event.delaying = false;
			} else {
				event.done = false;
				return;
			}
		}
		if (event.delay > 0) {
			event.delayStartTime = GameTime.total();
			event.delaying = true;
			event.done = false;
			return;
		}
		if (event.object != null) {
			if (!event.registered) {
				event.registered = true;
				GameObjectFactory.registerObject(event.object);
				event.object.x = event.startX > DisplaySize.gameWidth ?
						DisplaySize.gameWidth : event.startX;
				event.object.y = event.startY >  DisplaySize.gameHeight ?
						0 : event.startY;
				event.done = false;
			} else {
				if (event.object.isDead) {
					GameObjectFactory.release(event.object);
					event.object = null;
				} else {
					event.done = false;
				}
			}
		} // TODO: switch scenes!!
		// Note that top level events happen in order but child events are
		// processed at the same time. This can be useful for certain cases.
		if (event.children != null) {
			for (int i = 0; i < event.children.getCount(); i++) {
				LevelEvent currentChild = event.children.get(i);
				if (!currentChild.done) {
					processEvent(currentChild);
					event.done = false;
				}
			}
		}
	}

	/**
	 * Update the level by scrolling the background and processing events.
	 */
	public void update() {
		if (levelCompleted)
			return;

		currentScene = scenes.peek();
		renderer.draw();
		LevelEvent currentEvent = levelEvents.get(currentEventIndex);
		if (currentEvent.done) {
			currentEventIndex++;
			if (currentEventIndex >= levelEvents.getCount()) {
				levelCompleted = true;
			}
		}
		processEvent(currentEvent);
	}

	/**
	 *
	 * @return true if the level is completed.
	 */
	public boolean isCompleted() {
		return levelCompleted;
	}

	/**
	 *
	 * @return Current scene.
	 */
	public Scene getCurrentScene() {
		return currentScene;
	}

	/**
	 *
	 * @return Game instance.
	 */
	public Game getGame() {
		return game;
	}
}
