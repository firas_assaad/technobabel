package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.object.GameObject;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 *
 * Level events are read from the level description file and processed in order.
 * An event can represent objects such as enemies and power-ups, commands such
 * as changing the background or delaying, and can be nested as well.
 */
public class LevelEvent {
	/** Maximum number of child events. */
	private static final int MAXIMUM_CHILD_EVENTS = 8;
	/** File name of the image. */
	public String sprite;
	/** Starting X of the object (screen width by default). */
	public int startY;
	/** Starting Y of the object (0 by default). */
	public int startX;
	/** Delay in milliseconds. */
	public int delay;
	/** Is this a change background event? */
	public boolean backgroundImage;
	/** List of child events. */
	public FixedSizeArray<LevelEvent> children;
	/** The object spawned by this event. */
	public GameObject object;
	/** Is this event delaying right now? */
	public boolean delaying;
	/** When did the delay start, in milliseconds. */
	public long delayStartTime;
	/** Are we done with processing the event? */
	public boolean done;
	/** Was the event object registered? */
	public boolean registered;

	public LevelEvent() {
		startY = Integer.MAX_VALUE - 1;
		startX = Integer.MAX_VALUE - 1;
	}

	/**
	 * Add a child event.
	 *
	 * @param child
	 */
	public void addChild(LevelEvent child) {
		if (children == null)
			children = new FixedSizeArray<LevelEvent>(MAXIMUM_CHILD_EVENTS);
		children.add(child);
	}
}
