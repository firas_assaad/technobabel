package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.graphics.DisplaySize;
import com.ghostinabottle.technobabel.object.BasicObject;

/**
 * A component to track the lifetime of the object.
 */
public class LifeComponent extends GameComponent {
	/** Does the object explode before dying? */
	private boolean explode;

	public LifeComponent(BasicObject object) {
		super(object);
	}

	public LifeComponent(BasicObject object, boolean explode) {
		super(object);
		this.explode = explode;
	}

	/**
	 * Kill the object if its out of the screen or out of HP.
	 */
	@Override
	public void update() {
		if (basicObject.x + basicObject.width < 0 || basicObject.x > DisplaySize.gameWidth
				|| basicObject.y + basicObject.height < 0
				|| basicObject.y > DisplaySize.gameHeight) {
			basicObject.isDead = true;
		}
		if (basicObject.health <= 0) {
			if (!basicObject.isExploding)
			if (explode)
				basicObject.isExploding = true;
			else
				basicObject.isDead = true;
		}
	}

}
