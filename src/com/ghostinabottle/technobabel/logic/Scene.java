package com.ghostinabottle.technobabel.logic;

import com.ghostinabottle.technobabel.util.FixedSizeArray;

/** A scene describes a group of scrolling backgrounds and
 * is responsible for updating them. */
public final class Scene {
	private final static int MAX_NUMBER_OF_SPRITES = 20;
	public FixedSizeArray<BackgroundSprite> sprites;

	public Scene() {
		sprites = new FixedSizeArray<BackgroundSprite>(MAX_NUMBER_OF_SPRITES);
	}
}
