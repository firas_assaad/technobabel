package com.ghostinabottle.technobabel.logic;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.graphics.Frame;
import com.ghostinabottle.technobabel.graphics.Pose;
import com.ghostinabottle.technobabel.graphics.SpriteData;
import com.ghostinabottle.technobabel.util.FixedSizeArray;
import com.ghostinabottle.technobabel.util.GameTime;

/**
 *
 * A class that encapsulates sprite updating logic.
 */
public final class SpriteLogic {
	/** Data to act on. */
	private SpriteData spriteData;
	/** Currently active pose. */
	private Pose currentPose;
	/** Source rectangle of the current frame. */
	private Rect srcRect;
	/** Current frame index. */
	private int frameIndex;
	/** Used to check if enough time passed since last frame. */
	private long oldTime;
	/** Number of times animation repeated so far. */
	private int repeatNumber;
	/** Total number of frames. */
	private int frameCount;
	/** Is the pose finished? */
	private boolean finished;
	/** Is the animation in a tween frame? */
	private boolean tweening;

	public SpriteLogic() {
		srcRect = new Rect();
	}

	public SpriteLogic(SpriteData spriteData) {
		this();
		setSpriteData(spriteData);
	}

	public void setSpriteData(SpriteData spriteData) {
		this.spriteData = spriteData;
	}

	/**
	 *
	 * @return Sprite information.
	 */
	public SpriteData getSpriteData() {
		return spriteData;
	}

	/** Reset values to their defaults. */
	public void reset() {
		frameIndex = 0;
		oldTime = GameTime.total();
		repeatNumber = 0;
		frameCount = currentPose.frames.getCount();
		finished = false;
		tweening = false;
	}

	/**
	 * Set the current pose.
	 *
	 * @param poseName
	 *            Name of the new pose.
	 */
	public void setPose(String poseName) {
		FixedSizeArray<Pose> poses = spriteData.poses;
		if (currentPose == null || !poseName.equalsIgnoreCase(currentPose.name)) {
			int poseCount = poses.getCount();
			for (int i = 0; i < poseCount; i++) {
				if (poses.get(i).name.equalsIgnoreCase(poseName))
					currentPose = poses.get(i);
			}
		}
		reset();
	}

	/**
	 * Update the current pose.
	 */
	public void update() {
		if (currentPose == null)
			return;
		if (frameCount == 0)
			return;
		// If we number of repeats is reached then animation is finished
		if (currentPose.repeats != -1 && repeatNumber >= currentPose.repeats) {
			finished = true;
		}
		if (finished)
			return;

		// If animation is still not finished...
		if (currentPose.repeats == -1 || repeatNumber < currentPose.repeats) {
			Frame currentFrame = currentPose.frames.get(frameIndex);
			int frameDuration = currentFrame.duration;
			int frameTime = frameDuration == -1 ? currentPose.defaultDuration
					: frameDuration;
			if (!tweening && currentFrame.isTweenFrame) {
				Frame prevFrame = currentPose.frames.get(frameIndex - 1);
				currentFrame.x = prevFrame.x;
				currentFrame.y = prevFrame.y;
				currentFrame.width = prevFrame.width;
				currentFrame.height = prevFrame.height;
				oldTime = GameTime.total();
				tweening = true;
			}
			if (tweening) {
				Frame prevFrame = currentPose.frames.get(frameIndex - 1);
				Frame nextFrame = currentPose.frames.get(frameIndex + 1);
				float alpha = (float) (GameTime.total() - oldTime) / currentFrame.duration;
				alpha = Math.min(Math.max(alpha, 0.0f), 1.0f);
				currentFrame.xMagnification = lerp(prevFrame.xMagnification, nextFrame.xMagnification, alpha);
				currentFrame.yMagnification = lerp(prevFrame.yMagnification, nextFrame.yMagnification, alpha);
				currentFrame.angle= (int) lerp(prevFrame.angle, nextFrame.angle, alpha);
			}
			if (GameTime.passed(oldTime, frameTime)) {
				oldTime = GameTime.total();
				if (tweening)
					tweening = false;
				frameIndex++;
				if (frameIndex >= frameCount - 1) {
					repeatNumber++;
				}
				frameIndex %= frameCount;
			}
		}
	}

	/**
	 * Linear interpolation between two floats.
	 * @param start Starting value.
	 * @param end Ending value.
	 * @param alpha Current time.
	 * @return Linearly interpolated value at time alpha.
	 */
	private float lerp(float start, float end, float alpha) {
		return (1.0f - alpha) * start + alpha * end;
	}

	/**
	 * Get the current pose.
	 *
	 * @return Current pose.
	 */
	public Pose getCurrentPose() {
		return currentPose;
	}

	/**
	 *
	 * @return Collision bounding box.
	 */
	public BoundingBox getBoundingBox() {
		if (currentPose == null)
			return null;
		else
			return currentPose.boundingBox;
	}

	/**
	 *
	 * @return Current frame in the current pose.
	 */
	public Frame getCurrentFrame() {
		assert currentPose != null;
		return currentPose.frames.get(frameIndex);
	}

	/**
	 * Set the source rect. If you call it once it will be
	 * used until you call getSourceRect
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public void setSourceRect(int x, int y, int width, int height) {
		srcRect.set(x, y, x + width, x + height);
	}

	/**
	 *
	 * @return Source rectangle of the current frame.
	 */
	public Rect getSourceRect() {
		Frame frame = getCurrentFrame();
		srcRect.set(frame.x, frame.y, frame.x + frame.width, frame.y
				+ frame.height);
		return srcRect;
	}

	/** Is the current animation done? */
	public boolean isFinished() {
		return finished;
	}

	/** Stop updating the pose. */
	public void stop() {
		finished = true;
	}

}
