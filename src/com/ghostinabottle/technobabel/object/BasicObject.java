package com.ghostinabottle.technobabel.object;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.logic.BoundingBox;

/**
 * Parent class to objects such as game objects or bullets.
 */
public abstract class BasicObject {
	/** The game object. */
	protected Game game;
	/** Object type. */
	public ObjectType type;
	/** Collision bounding box. */
	public BoundingBox boundingBox;
	/** X coordinate of the object. */
	public float x;
	/** Y coordinate of the object. */
	public float y;
	/** Object's layer (Z coordinate). */
	public int layer;
	/** Width of the object in pixels. */
	public int width;
	/** Height of the object in pixels. */
	public int height;
	/** Horizontal velocity of the object. */
	public float xVelocity;
	/** Vertical velocity of the object. */
	public float yVelocity;
	/** Amount of health. */
	public int health;
	/** Should the object be destroyed? */
	public boolean isDead;
	/** If true then the object should start exploding. */
	public boolean isExploding;
	/** Is the objective active in the game or just loaded and waiting? */
	public boolean isActive;

	/**
	 *
	 * @param game
	 *            Game object.
	 */
	public BasicObject(Game game) {
		this.game = game;
		layer = 3;
	}

	/**
	 * Update the object.
	 */
	public abstract void update();

	/**
	 * Reset object to default state.
	 */
	public void reset() {
		x = y = 0.0f;
		layer = 3;
		xVelocity = yVelocity = 0.0f;
		isDead = false;
		isActive = false;
	}

	/**
	 * Get the game instance (used by components).
	 *
	 * @return The game instance.
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Sets the game instance (a hack for pool!)
	 *
	 * @param game
	 */
	public void setGame(Game game) {
		this.game = game;
	}
}
