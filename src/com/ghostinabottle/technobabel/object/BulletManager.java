package com.ghostinabottle.technobabel.object;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.R;
import com.ghostinabottle.technobabel.graphics.BulletRenderer;
import com.ghostinabottle.technobabel.logic.ComputerMoverComponent;
import com.ghostinabottle.technobabel.logic.FacingDirection;
import com.ghostinabottle.technobabel.logic.ForwardMoverComponent;
import com.ghostinabottle.technobabel.object.ObjectType;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * This class is responsible for creating and managing bullet objects.
 */
public final class BulletManager {
	/** Game instance used to create bullets. */
	private static Game game;
	/** Maximum number of bullets. */
	private final static int MAX_NUMBER_OF_BULLETS = 50;
	/** Pool of bullets. */
	private static BulletPool bulletPool;
	/** The shared bullet renderer. */
	private static BulletRenderer renderer;
	/** The shared mover component. */
	private static ComputerMoverComponent mover;
	/** List of active bullets. */
	private static FixedSizeArray<BulletObject> gameBullets;

	/**
	 * Initialize the bullet factory.
	 *
	 * @param game
	 *            Game instance.
	 */
	public static void set(Game game) {
		BulletManager.game = game;
		renderer = new BulletRenderer(game, R.drawable.bullets);
		mover = new ForwardMoverComponent(null, null, 0.0f);
		bulletPool = new BulletPool(MAX_NUMBER_OF_BULLETS);
		gameBullets = new FixedSizeArray<BulletObject>(MAX_NUMBER_OF_BULLETS);
	}

	/**
	 * Create a bullet from the player.
	 *
	 * @return Created bullet object.
	 */
	public static BulletObject createPlayerBullet(int id) {
		BulletObject object = (BulletObject) bulletPool.allocate();
		// TODO: use some sort of bullet description file
		object.health = 1;
		object.type = ObjectType.FRIENDLY;
		object.isActive = true;
		int spriteId = 0;
		if (id == 0) {
			spriteId = R.raw.bullet;
		}
		object.setComponents(game, renderer, mover);
		object.setSpriteData(spriteId);
		object.direction = FacingDirection.FACE_RIGHT;
		gameBullets.add(object);
		return object;
	}

	/**
	 * Called every frame to update bullet objects and remove dead bullets.
	 *
	 */
	public static void update() {
		int bulletsSize = gameBullets.getCount();
		BulletObject bullet;
		for (int i = 0; i < bulletsSize; ) {
			bullet = gameBullets.get(i);
			if (bullet.isDead) {
				release(bullet);
				gameBullets.swapWithLast(i);
				gameBullets.removeLast();
				bulletsSize = gameBullets.getCount();
			} else {
				bullet.update();
				i++;
			}
		}
	}

	/**
	 * Releases the object to be reused.
	 *
	 * @param object
	 *            Bullet Object to release.
	 */
	private static void release(BulletObject object) {
		bulletPool.release(object);
	}

}
