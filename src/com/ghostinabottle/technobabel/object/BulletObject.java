package com.ghostinabottle.technobabel.object;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.BulletRenderer;
import com.ghostinabottle.technobabel.graphics.SpriteData;
import com.ghostinabottle.technobabel.logic.CollisionComponent;
import com.ghostinabottle.technobabel.logic.ComputerMoverComponent;
import com.ghostinabottle.technobabel.logic.FacingDirection;
import com.ghostinabottle.technobabel.logic.LifeComponent;
import com.ghostinabottle.technobabel.logic.SpriteLogic;

/**
 * A lightweight object representing a single bullet.
 */
public class BulletObject extends BasicObject {
	/** The renderer. Shared with other bullets. */
	private BulletRenderer renderer;
	/** The mover. Shared with other bullets. */
	private ComputerMoverComponent moverComponent;
	/** The collision component. */
	private CollisionComponent collisionComponent;
	/** The life checking component. */
	private LifeComponent lifeComponent;
	/** Sprite updater. */
	private SpriteLogic spriteLogic;
	/** Destination rectangle. */
	private Rect destRect;

	/** Bullet's facing direction. */
	public FacingDirection direction;

	public BulletObject(Game game) {
		super(game);
		lifeComponent = new LifeComponent(this);
		destRect = new Rect();
		spriteLogic = new SpriteLogic();
	}

	/**
	 * Set the components. Called when creating the bullet.
	 *
	 * @param game
	 *            Game instance.
	 * @param renderer
	 *            Bullet renderer.
	 * @param mover
	 *            Bullet mover.
	 */
	public void setComponents(Game game, BulletRenderer renderer,
			ComputerMoverComponent mover) {
		this.game = game;
		this.renderer = renderer;
		this.moverComponent = mover;
		this.collisionComponent = game.getCollisionDetector()
				.createCollisionComponent(this);
	}

	/**
	 * Set the associated sprite data.
	 *
	 * @param spriteId
	 */
	public void setSpriteData(int spriteId) {
		SpriteData spriteData;
		spriteData = game.getSpriteDataFactory().allocateSpriteData(spriteId);
		spriteLogic.setSpriteData(spriteData);
		spriteLogic.setPose("Main");
		spriteLogic.reset();
	}

	/**
	 * Called before bullet is returned to the pool.
	 */
	@Override
	public void reset() {
		super.reset();
		this.renderer = null;
		this.moverComponent = null;
		collisionComponent.dispose();
	}

	/**
	 * Update the components and draw the bullet.
	 */
	@Override
	public void update() {
		if (isDead) {
			return;
		}
		lifeComponent.update();
		moverComponent.set(this, direction, 5.0f);
		moverComponent.update();
		collisionComponent.update();
		spriteLogic.update();
		Rect srcRect = spriteLogic.getSourceRect();
		width = srcRect.width();
		height = srcRect.height();
		float xMag = spriteLogic.getCurrentFrame().xMagnification;
		float yMag = spriteLogic.getCurrentFrame().yMagnification;
		int destWidth = (int) (xMag * srcRect.width());
		int destHeight = (int) (yMag * srcRect.height());
		destRect.set((int) x, (int) y,
				(int) x + destWidth, (int) y + destHeight);
		renderer.set(srcRect, destRect);
		renderer.draw();
	}

}
