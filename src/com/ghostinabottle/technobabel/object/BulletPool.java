package com.ghostinabottle.technobabel.object;

/**
 * A pool of bullets.
 */
public class BulletPool extends ObjectPool {

	public BulletPool(int size) {
		super(size);
	}

	/**
	 * Fill the pool with bullets.
	 */
	@Override
	protected void fill() {
		int size = getSize();
		for (int i = 0; i < size; i++) {
			getAvailable().add(new BulletObject(null));
		}
	}

	/** Returns a bullet to the pool. */
	@Override
	public void release(Object entry) {
		// Clear the object to its default state
		BulletObject object = (BulletObject) entry;
		object.reset();
		super.release(object);
	}

}
