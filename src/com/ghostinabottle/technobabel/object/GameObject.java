package com.ghostinabottle.technobabel.object;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.logic.GameComponent;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * A game object is an extensible entity that can hold several components. When
 * a game object is updated all its component are updated as well.
 */
public class GameObject extends BasicObject {
	/** Array of components. */
	private FixedSizeArray<GameComponent> components;
	/** Maximum number of components per object. */
	public final static int MAX_NUMBER_OF_COMPONENTS = 8;
	/** If true then the object is shooting. */
	public boolean isShooting;

	public GameObject(Game game) {
		super(game);
		this.components = new FixedSizeArray<GameComponent>(
				MAX_NUMBER_OF_COMPONENTS);
	}

	/**
	 * Add a component to the object.
	 *
	 * @param component
	 *            Component to add.
	 */
	public void addComponent(GameComponent component) {
		components.add(component);
	}

	/**
	 * Remove all components attached to this object.
	 */
	public void clearComponents() {
		int count = components.getCount();
		for (int i = 0; i < count; i++) {
			components.get(i).dispose();
		}
		components.clear();
	}

	/**
	 * Reset object to default state.
	 */
	@Override
	public void reset() {
		game = null;
		x = y = 0.0f;
		xVelocity = yVelocity = 0.0f;
		isDead = false;
		isExploding = false;
		health = 0;
		clearComponents();
	}

	/** Update all components. */
	@Override
	public void update() {
		if (isDead) {
			return;
		}
		for (int i = 0; i < components.getCount(); i++) {
			components.get(i).update();
		}
	}

}
