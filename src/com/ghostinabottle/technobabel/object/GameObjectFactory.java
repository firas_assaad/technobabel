package com.ghostinabottle.technobabel.object;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.SpriteComponent;
import com.ghostinabottle.technobabel.input.InputMoverComponent;
import com.ghostinabottle.technobabel.logic.BulletEmitterComponent;
import com.ghostinabottle.technobabel.logic.CollisionComponent;
import com.ghostinabottle.technobabel.logic.FacingDirection;
import com.ghostinabottle.technobabel.logic.ForwardMoverComponent;
import com.ghostinabottle.technobabel.logic.LifeComponent;
import com.ghostinabottle.technobabel.util.FixedSizeArray;
import com.ghostinabottle.technobabel.object.ObjectType;

/**
 * This class is responsible for creating all types of objects.
 */
public class GameObjectFactory {
	/** Game instance used to create game objects. */
	private static Game game;
	/** Pool of game objects. */
	private static GameObjectPool objectPool;
	/** List of active game objects. */
	private static FixedSizeArray<GameObject> gameObjects;

	/**
	 * Initialize the game object factory.
	 *
	 * @param game
	 *            Game instance.
	 * @param size
	 *            Maximum number of objects.
	 */
	public static void set(Game game, int size) {
		GameObjectFactory.game = game;
		objectPool = new GameObjectPool(size);
		gameObjects = new FixedSizeArray<GameObject>(size);
		BulletManager.set(game);
	}

	/**
	 * Create and return a new game object.
	 * @param sprite sprite name.
	 * @param register should the object be registered right away?
	 * @return the created object.
	 */
	public static GameObject createGameObject(String sprite, boolean register) {
		// TODO : Create object from object description file
		GameObject object = null;
		int spriteId = game.getResourceManager().getResource(sprite, "raw");
		if (sprite.equalsIgnoreCase("player")) {
			object = createPlayer(spriteId);

		}
		else {
			object = createHorizontalImpactEnemy(spriteId);
		}
		if (register) {
			registerObject(object);
		}
		return object;
	}

	/**
	 * Creates and returns a new bullet.
	 * @param name a name that identifies the type of bullet.
	 * @return the created bullet.
	 */
	public static BulletObject createBullet(String name) {
		BulletObject bullet = null;
		 if (name.equalsIgnoreCase("player bullet")) {
				bullet = createPlayerBullet();
		 }
		 return bullet;
	}

	/**
	 * When objects are created they are not part of the game
	 * until they are later registered using this method.
	 * @param object
	 */
	public static void registerObject(GameObject object) {
		object.isActive = true;
		gameObjects.add(object);
	}

	/**
	 * Create a player object with given sprite.
	 *
	 * @param spriteID
	 *            Resource ID of the sprite data.
	 * @return Created player game object.
	 */
	private static GameObject createPlayer(int spriteID) {
		GameObject object = (GameObject) objectPool.allocate();
		object.setGame(game);
		object.type = ObjectType.FRIENDLY;
		object.addComponent(new SpriteComponent(object, spriteID));
		object.addComponent(new InputMoverComponent(object));
		BulletEmitterComponent emitter = new BulletEmitterComponent(object, 500);
		emitter.setOffsets(object.width, object.height / 2);
		object.addComponent(emitter);
		CollisionComponent collisionComponent;
		collisionComponent = game.getCollisionDetector()
				.createCollisionComponent(object);
		object.addComponent(collisionComponent);
		return object;
	}

	/**
	 * Create a simple enemy that moves forward and doesn't attack.
	 *
	 * @param spriteID
	 *            Resource ID of the sprite data.
	 * @return Created enemy game object.
	 */
	private static GameObject createHorizontalImpactEnemy(int spriteID) {
		GameObject object = (GameObject) objectPool.allocate();
		object.setGame(game);
		object.health = 2;
		object.type = ObjectType.HOSTILE;
		object.addComponent(new SpriteComponent(object, spriteID));
		FacingDirection direction = FacingDirection.FACE_LEFT;
		object.addComponent(new ForwardMoverComponent(object, direction, 3.0f));
		CollisionComponent collisionComponent;
		collisionComponent = game.getCollisionDetector()
				.createCollisionComponent(object);
		object.addComponent(collisionComponent);
		object.addComponent(new LifeComponent(object, true));
		return object;
	}

	/**
	 * Create a bullet from the player. Bullets are not true game objects
	 * and are managed by the bullet factory.
	 *
	 * @return Created bullet game object.
	 */
	private static BulletObject createPlayerBullet() {
		BulletObject object = BulletManager.createPlayerBullet(0);
		return object;
	}

	/**
	 * Releases the object to be reused.
	 *
	 * @param object
	 *            Game Object to release.
	 */
	public static void release(GameObject object) {
		objectPool.release(object);
		int index = gameObjects.find(object, true);
		gameObjects.swapWithLast(index);
		gameObjects.removeLast();
	}

	/**
	 *
	 * @return Array of active game objects
	 */
	public static FixedSizeArray<GameObject> getGameObjects() {
		return gameObjects;
	}
}
