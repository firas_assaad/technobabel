package com.ghostinabottle.technobabel.object;

/**
 * A pool of game objects.
 */
public class GameObjectPool extends ObjectPool {

	public GameObjectPool(int size) {
		super(size);
	}

	/**
	 * Fill the pool with game objects that have no components.
	 */
	@Override
	protected void fill() {
		int size = getSize();
		for (int i = 0; i < size; i++) {
			// Note that fill is called in parent constructor
			getAvailable().add(new GameObject(null));
		}
	}

	/** Returns an object to the pool. */
	@Override
	public void release(Object entry) {
		// Clear the object to its default state
		GameObject object = (GameObject) entry;
		object.reset();
		super.release(object);
	}

}
