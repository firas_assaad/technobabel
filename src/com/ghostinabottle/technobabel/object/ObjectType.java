package com.ghostinabottle.technobabel.object;

/**
 * An enumeration representing the different types of objects.
 */
public enum ObjectType {
	/** Player or assist character. */
	FRIENDLY,
	/** Enemy or boss. */
	HOSTILE,
	/** Neutral objects such as rocks. */
	NEUTRAL,
	/** Power up item. */
	POWERUP
}
