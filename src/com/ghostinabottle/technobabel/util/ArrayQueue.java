package com.ghostinabottle.technobabel.util;

/**
 * An array implementation of the queue data structure.
 *
 * @param <T>
 *            Contained object type.
 */
public final class ArrayQueue<T> {
	/** Underlying array. */
	private final T[] contents;
	/** Head of the queue. */
	private int head;
	/** Tail of the queue. */
	private int tail;
	/** Maximum size. */
	private int size;

	@SuppressWarnings("unchecked")
	public ArrayQueue(int size) {
		assert size > 0;
		contents = (T[]) new Object[size];
		head = tail = 0;
		this.size = size;
	}

	/**
	 * Add an element to the end of the queue.
	 *
	 * @param element
	 *            Element to add.
	 */
	public void enqueue(T element) {
		assert (tail + 1) % size != head : "Queue is full!";
		contents[tail] = element;
		tail = (tail + 1) % size;
	}

	/**
	 * Get and remove the element at the beginning of the queue.
	 *
	 * @return Element at the beginning of the queue.
	 */
	public T dequeue() {
		assert tail != head : "Empty queue!";
		T value = contents[head];
		head = (head + 1) % size;
		return value;
	}

	/**
	 * Get but don't remove the element at the beginning of the queue.
	 *
	 * @return Element at the beginning of the queue.
	 */
	public T peek() {
		return contents[head];
	}

	/**
	 *
	 * @return True if the queue is empty.
	 */
	public boolean empty() {
		return head == tail;
	}

	/**
	 * Empty the queue.
	 */
	public void clear() {
		head = tail = 0;
	}

}
