package com.ghostinabottle.technobabel.util;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.ghostinabottle.technobabel.Game;
import com.ghostinabottle.technobabel.graphics.SpriteData;
import com.ghostinabottle.technobabel.graphics.SpriteRenderer;
import com.ghostinabottle.technobabel.graphics.Texture;
import com.ghostinabottle.technobabel.logic.BackgroundSprite;
import com.ghostinabottle.technobabel.logic.Level;
import com.ghostinabottle.technobabel.logic.Scene;
import com.ghostinabottle.technobabel.logic.SpriteLogic;

/** A SAX handler used to read background data XML files. */
public class BackgroundSaxHandler  extends DefaultHandler{
	/** Level to add scenes to*/
	private Level level;
	/** Current scene. */
	private Scene currentScene;
	/** Current background sprite. */
	private BackgroundSprite currentSprite;
	// inX is true if inside <X> </X> tag
	private boolean inScene;
	private boolean inSprite;
	private boolean inSpriteId;
	private boolean inSpriteLayer;
	private boolean inSpriteXVelocity;
	private boolean inSpriteYVelocity;
	private boolean inSpriteStartX;
	private boolean inSpriteStartY;

	public BackgroundSaxHandler(Level level) {
		this.level = level;
	}

	@Override
	public void startDocument() throws SAXException {
	}

	@Override
	public void endDocument() throws SAXException {
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (localName.equalsIgnoreCase("scene")) {
			inScene = true;
			currentScene = new Scene();
		}
		if (localName.equalsIgnoreCase("background") ||
				localName.equalsIgnoreCase("sprite")) {
			inSprite = true;
			currentSprite = new BackgroundSprite();
			currentSprite.isBackground =
				localName.equalsIgnoreCase("background");
		}
		if (localName.equalsIgnoreCase("layer")) {
			inSpriteLayer = true;
		}
		if (localName.equalsIgnoreCase("xVelocity")) {
			inSpriteXVelocity = true;
		}
		if (localName.equalsIgnoreCase("yVelocity")) {
			inSpriteYVelocity = true;
		}
		if (localName.equalsIgnoreCase("startX")) {
			inSpriteStartX = true;
		}
		if (localName.equalsIgnoreCase("startY")) {
			inSpriteStartY = true;
		}
		if (localName.equalsIgnoreCase("id")) {
			inSpriteId = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equalsIgnoreCase("scene")) {
			inScene = false;
			level.addScene(currentScene);
		}
		if (localName.equalsIgnoreCase("layer")) {
			inSpriteLayer = false;
		}
		if (localName.equalsIgnoreCase("xVelocity")) {
			inSpriteXVelocity = false;
		}
		if (localName.equalsIgnoreCase("yVelocity")) {
			inSpriteYVelocity = false;
		}
		if (localName.equalsIgnoreCase("background") ||
				localName.equalsIgnoreCase("sprite")) {
			inSprite = false;
			currentScene.sprites.add(currentSprite);
		}
		if (localName.equalsIgnoreCase("startX")) {
			inSpriteStartX = false;
		}
		if (localName.equalsIgnoreCase("startY")) {
			inSpriteStartY = false;
		}
		if (localName.equalsIgnoreCase("id")) {
			inSpriteId = false;
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		String str;
		if (inScene) {
			if (inSprite) {
				if (inSpriteLayer) {
					str = new String(ch, start, length);
					currentSprite.layer = Integer.parseInt(str);
				}
				if (inSpriteXVelocity) {
					str = new String(ch, start, length);
					currentSprite.xVelocity = Integer.parseInt(str);
				}
				if (inSpriteYVelocity) {
					str = new String(ch, start, length);
					currentSprite.yVelocity = Integer.parseInt(str);
				}
				if (inSpriteStartX) {
					str = new String(ch, start, length);
					currentSprite.startX = Integer.parseInt(str);
					currentSprite.x = currentSprite.startX;
				}
				if (inSpriteStartY) {
					str = new String(ch, start, length);
					currentSprite.startY = Integer.parseInt(str);
					currentSprite.y = currentSprite.startY;
				}
				if (inSpriteId) {
					Game game = level.getGame();
					str = new String(ch, start, length);
					ResourceManager rm = game.getResourceManager();
					int id = rm.getResource(str, "raw");
					SpriteData spriteData =
					game.getSpriteDataFactory().allocateSpriteData(id);
					SpriteLogic spriteLogic =
						new SpriteLogic(spriteData);
					spriteLogic.setPose("Main");
					int img =
						rm.getResource(spriteData.image, "drawable");
					Texture texture =
						game.getTextureFactory().allocateTexture(img);
					currentSprite.renderer =
						new SpriteRenderer(game.getDisplayManager());
					currentSprite.renderer.set(spriteLogic, texture);
				}
			}
		}
	}
}

