package com.ghostinabottle.technobabel.util;

/**
 * A command is something that can be processed.
 */
public abstract class Command {
	/**
	 * Process the command.
	 */
	public abstract void process();
}
