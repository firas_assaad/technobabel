package com.ghostinabottle.technobabel.util;

/** A thread safe queue of commands. */
public final class CommandQueue {
	/** The underlying queue. */
	private ArrayQueue<Command> queue;

	/**
	 * Create a command queue with a given capacity.
	 *
	 * @param size
	 *            Maximum number of commands.
	 */
	public CommandQueue(int size) {
		queue = new ArrayQueue<Command>(size);
	}

	/**
	 * Add a command to the queue.
	 *
	 * @param command
	 *            Command to add.
	 */
	public synchronized void enqueue(Command command) {
		queue.enqueue(command);
	}

	/**
	 * Empty the queue.
	 */
	public synchronized void clear() {
		queue.clear();
	}

	/**
	 * Retrieve the head of the queue without removing it.
	 *
	 * @return Head of the queue.
	 */
	public synchronized Command peek() {
		return queue.peek();
	}

	/**
	 * Retrieve the head of the queue and remove it.
	 *
	 * @return Head of the queue.
	 */
	public synchronized Command dequeue() {
		return queue.dequeue();
	}

	/**
	 *
	 * @return True if queue is empty.
	 */
	public boolean empty() {
		return queue.empty();
	}
}
