package com.ghostinabottle.technobabel.util;

/** A class for common utility functions. */
public final class Common {
	
	/**
	 * 
	 * @param number
	 * @return next power of two of a number
	 */
	public static int nextPowerOfTwo(int number) {
		// Replace the number in binary with all 1s then add 1 for next POT
		int next = number;
		// For the special case of number already being POT
		next--;
		next |= next >> 1;
		next |= next >> 2;
		next |= next >> 4;
		next |= next >> 8;
		next |= next >> 16;
		return next + 1;
	}
}
