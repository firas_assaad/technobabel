package com.ghostinabottle.technobabel.util;

import android.os.SystemClock;

/** Utility class for managing time. */
public final class GameTime {

	/** Total number of frames since game start. */
	private static int totalFrames = 0;
	/** Last FPS update time. */
	private static long lastUpdateTime = 0;
	
	/** Frames per second. */
	public static int fps = 0;

	/** Total system up time. */
	public static long total() {
		return SystemClock.uptimeMillis();
	}

	/** Check if 'time' milliseconds has passed since the 'old' time. */
	public static boolean passed(long old, int time) {
		return total() - old >= time;
	}

	/** Update the time system to keep track of total frames and fps. */
	public static void update() {
		totalFrames++;
		if (passed(lastUpdateTime, 1000)) {
			lastUpdateTime = total();
			fps = totalFrames;
			totalFrames = 0;
		}
	}

}
