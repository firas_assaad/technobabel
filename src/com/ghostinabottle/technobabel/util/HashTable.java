package com.ghostinabottle.technobabel.util;

/**
 *
 * Simple array-based hash table implementation. The key should not be null and
 * should provide implementations for hashCode and equals functions. The table
 * is populated on creation to minimize garbage collection overhead.
 *
 * @param <S>
 *            Key type.
 * @param <T>
 *            Value type.
 */
public final class HashTable<S, T> {
	/** The hash table. */
	private Entry<S, T>[] table;
	/** A null entry used for finding positions for new values. */
	private final Entry<S, T> nullEntry = new Entry<S, T>();
	/** An entry used for finding existing keys. */
	private final Entry<S, T> keyEntry = new Entry<S, T>();

	/**
	 * Construct a hash table with specified maximum size.
	 *
	 * @param size
	 */
	@SuppressWarnings("unchecked")
	public HashTable(int size) {
		table = (Entry<S, T>[]) new Entry[size];
		// Populate the table with unset entries
		for (int i = 0; i < table.length; i++) {
			table[i] = new Entry<S, T>();
		}
	}

	/**
	 * Create a new key/value mapping and insert it into the table.
	 *
	 * @param key
	 * @param value
	 */
	public void put(S key, T value) {
		int index = findPosition(hash(key), nullEntry);
		assert index != -1 : "HashTable is full!";
		table[index].set(key, value);
	}

	/**
	 *
	 * @param key
	 * @return The value with the specified key or null if it's not found.
	 */
	public T get(S key) {
		keyEntry.set(key, null);
		int index = findPosition(hash(key), keyEntry);
		if (index == -1) {
			return null;
		} else {
			return table[index].value;
		}
	}

	/**
	 *
	 * @param key
	 * @return True if key exists in the hash table.
	 */
	public boolean contains(S key) {
		keyEntry.set(key, null);
		int index = findPosition(hash(key), keyEntry);
		return index != -1;
	}

	/**
	 * Very simple hash function.
	 *
	 * @param key
	 * @return Hash value for key.
	 */
	public int hash(S key) {
		return Math.abs(key.hashCode()) % table.length;
	}

	/**
	 * Find the position of an entry starting from given index. If entry is a
	 * null entry (unset), a new empty position is found. Otherwise, the hash
	 * table is searched for a matching entry based on key comparison.
	 *
	 * @param index
	 *            Starting index to search from, normally the hash of the key.
	 * @param entry
	 *            Entry to search for.
	 * @return Index of entry in the table or -1 if entry isn't found or there's
	 *         no space for a new entry.
	 */
	private int findPosition(int index, Entry<S, T> entry) {
		// Loop until a position is found or entire table is searched
		int skipped = 0;
		while (skipped != table.length) {
			// If current entry isn't set
			if (table[index].isNull()) {
				// And we're inserting a entry, this is where to insert
				if (entry.isNull()) {
					return index;
				}
				// But if looking for a specific entry, we didn't find it
				else {
					return -1;
				}
			}
			// If current entry is set then we compare keys
			else if (table[index].key.equals(entry.key)) {
				return index;
			}
			// Move to next entry
			index = (index + 1) % table.length;
			skipped++;
		}
		// Searched the entire table but entry wasn't found or table is full
		return -1;
	}

	/**
	 *
	 * A single entry in the hash table.
	 *
	 * @param <S>
	 *            Key type.
	 * @param <T>
	 *            Value type.
	 */
	static class Entry<S, T> {
		/** Is the entry set or not? */
		private boolean unset;
		/** Entry's key. */
		public S key;
		/** Entry's value. */
		public T value;

		/** Create an unset entry. */
		public Entry() {
			unset = true;
		}

		/**
		 * Set the key and value.
		 *
		 * @param key
		 * @param value
		 */
		public void set(S key, T value) {
			this.key = key;
			this.value = value;
			unset = false;
		}

		/**
		 * Reset the entry and mark it as unset.
		 */
		public void reset() {
			key = null;
			value = null;
			unset = true;
		}

		/**
		 *
		 * @return True if the entry wasn't set.
		 */
		public boolean isNull() {
			return unset;
		}
	}
}
