package com.ghostinabottle.technobabel.util;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.ghostinabottle.technobabel.logic.Level;
import com.ghostinabottle.technobabel.logic.LevelEvent;
import com.ghostinabottle.technobabel.object.GameObjectFactory;
import com.ghostinabottle.technobabel.util.FixedSizeArray;

/**
 * A SAX handler to read level XML files.
 */
public class LevelSaxHandler extends DefaultHandler {
	/** Level to load. This level is modified as the XML file is parsed. */
	private Level level;
	// inX is true if inside <X></X> tags.
	private boolean inEvent;
	private boolean inEventSprite;
	private boolean inEventStartY;
	private boolean inEventStartX;
	private boolean inEventDelay;
	private boolean inEventBackground;

	/** The current level event. */
	private LevelEvent currentEvent;

	/** How nested can level events be? */
	private final static int NESTING_DEPTH = 3;
	/** Stack of level events used for nesting. */
	private FixedSizeArray<LevelEvent> eventStack;

	public LevelSaxHandler(Level level) {
		this.level = level;
		eventStack = new FixedSizeArray<LevelEvent>(NESTING_DEPTH);
	}

	@Override
	public void startDocument() throws SAXException {

	}

	@Override
	public void endDocument() throws SAXException {

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (localName.equalsIgnoreCase("event")) {
			inEvent = true;
			currentEvent = new LevelEvent();
			eventStack.add(currentEvent);
		}
		if (localName.equalsIgnoreCase("sprite")) {
			inEventSprite = true;
		}
		if (localName.equalsIgnoreCase("startY")) {
			inEventStartY = true;
		}
		if (localName.equalsIgnoreCase("startX")) {
			inEventStartX = true;
		}
		if (localName.equalsIgnoreCase("delay")) {
			inEventDelay = true;
		}
		if (localName.equalsIgnoreCase("background")) {
			inEventBackground = true;
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equalsIgnoreCase("event")) {
			currentEvent = eventStack.removeLast();
			if (eventStack.empty()) {
				level.addGameEvent(currentEvent);
				inEvent = false;
			} else {
				eventStack.last().addChild(currentEvent);
			}
			currentEvent = eventStack.last();
		}
		if (localName.equalsIgnoreCase("sprite")) {
			inEventSprite = false;
		}
		if (localName.equalsIgnoreCase("startY")) {
			inEventStartY = false;
		}
		if (localName.equalsIgnoreCase("startX")) {
			inEventStartX = false;
		}
		if (localName.equalsIgnoreCase("delay")) {
			inEventDelay = false;
		}
		if (localName.equalsIgnoreCase("background")) {
			inEventBackground = false;
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		if (inEvent) {
			if (inEventSprite) {
				currentEvent.sprite = new String(ch, start, length);
				currentEvent.object =
					GameObjectFactory.createGameObject(currentEvent.sprite, false);
			}
			if (inEventStartY) {
				currentEvent.startY = Integer.parseInt(new String(ch, start,
						length));
			}
			if (inEventStartX) {
				currentEvent.startX = Integer.parseInt(new String(ch, start,
						length));
			}
			if (inEventDelay) {
				currentEvent.delay = Integer.parseInt(new String(ch, start,
						length));
			}
			if (inEventBackground) {
				//String backgroundName = new String(ch, start, length);
				// TODO: read scene changes from level file 
				currentEvent.backgroundImage = true;
			}
		}
	}

}
