package com.ghostinabottle.technobabel.util;

import android.content.res.Resources;

/**
 * A hash table linking resource names to their identifiers.
 *
 */
public final class ResourceManager {
	private static final int MAX_NUMBER_OF_RESOURCES = 50;
	/** Table of loaded resources */
	private HashTable<String, Integer> resourceTable;
	/** Reference to Resources object */
	private Resources resources;
	/** Name of this package */
	private String packageName;

	/**
	 *
	 * @param r The Resources object of the context
	 * @param p The name of the package
	 */
	public ResourceManager(Resources r, String p) {
		resourceTable = new HashTable<String, Integer>(MAX_NUMBER_OF_RESOURCES);
		resources = r;
		packageName = p;
	}

	/**
	 *
	 * @param name Name of resource
	 * @param type Type of resource (e.g. drawable)
	 * @return The resource id of the resource with given name
	 */
	public int getResource(String name, String type) {
		DebugLog.d("RM", "Getting name: " + name + " type: " + type);
		Integer resource = resourceTable.get(type + name);
		if (resource == null) {
			resource = resources.getIdentifier(name, type, packageName);
			resourceTable.put(type + name, resource);
		}
		return resource.intValue();
	}
}
