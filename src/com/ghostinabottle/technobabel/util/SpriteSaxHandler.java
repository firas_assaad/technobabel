package com.ghostinabottle.technobabel.util;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.graphics.Rect;

import com.ghostinabottle.technobabel.graphics.Frame;
import com.ghostinabottle.technobabel.graphics.Pose;
import com.ghostinabottle.technobabel.graphics.SpriteData;
import com.ghostinabottle.technobabel.logic.BoundingBox;

/** A SAX handler used to read sprite data XML files. */
public class SpriteSaxHandler extends DefaultHandler {
	/** The sprite being read. */
	private SpriteData spriteData;
	// inX is true if inside <X> </X> tag
	private boolean inImage;
	private boolean inExplosion;
	private boolean inPose;
	private boolean inPoseName;
	private boolean inBoundingBox;
	private boolean inBoundingBoxX;
	private boolean inBoundingBoxY;
	private boolean inBoundingBoxWidth;
	private boolean inBoundingBoxHeight;
	private boolean inPoseDuration;
	private boolean inPoseRepeats;
	private boolean inFrame;
	private boolean inFrameX;
	private boolean inFrameY;
	private boolean inFrameWidth;
	private boolean inFrameHeight;
	private boolean inFrameMagX;
	private boolean inFrameMagY;
	private boolean inFrameDuration;
	private boolean inFrameAngle;
	/** Pose being read. */
	private Pose currentPose;
	/** Frame being read. */
	private Frame currentFrame;
	/** BoundingBox being read. */
	private BoundingBox currentBoundingBox;

	@Override
	public void startDocument() throws SAXException {
		spriteData = new SpriteData();
	}

	@Override
	public void endDocument() throws SAXException {

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (localName.equalsIgnoreCase("explosion")) {
			inExplosion = true;
		}
		if (localName.equalsIgnoreCase("image")) {
			inImage = true;
		}
		if (localName.equalsIgnoreCase("pose")) {
			inPose = true;
			currentPose = new Pose();
		}
		if (localName.equalsIgnoreCase("name")) {
			inPoseName = true;
		}
		if (localName.equalsIgnoreCase("bounding-box")) {
			inBoundingBox = true;
			currentBoundingBox = new BoundingBox();
		}
		if (localName.equalsIgnoreCase("duration")) {
			if (inFrame)
				inFrameDuration = true;
			else
				inPoseDuration = true;
		}
		if (localName.equalsIgnoreCase("repeats")) {
			inPoseRepeats = true;
		}
		if (localName.equalsIgnoreCase("tween")) {
			inFrame = true;
			currentFrame = new Frame();
			currentFrame.isTweenFrame = true;
		}
		if (localName.equalsIgnoreCase("frame")) {
			inFrame = true;
			currentFrame = new Frame();
		}
		if (localName.equalsIgnoreCase("x")) {
			if (inFrame)
				inFrameX = true;
			else if (inBoundingBox)
				inBoundingBoxX = true;
		}
		if (localName.equalsIgnoreCase("y")) {
			if (inFrame)
				inFrameY = true;
			else if (inBoundingBox)
				inBoundingBoxY = true;
		}
		if (localName.equalsIgnoreCase("width")) {
			if (inFrame)
				inFrameWidth = true;
			else if (inBoundingBox)
				inBoundingBoxWidth = true;
		}
		if (localName.equalsIgnoreCase("height")) {
			if (inFrame)
				inFrameHeight = true;
			else if (inBoundingBox)
				inBoundingBoxHeight = true;
		}
		if (localName.equalsIgnoreCase("x-magnification")) {
			inFrameMagX = true;
		}
		if (localName.equalsIgnoreCase("y-magnification")) {
			inFrameMagY = true;
		}
		if (localName.equalsIgnoreCase("angle")) {
			inFrameAngle = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equalsIgnoreCase("explosion")) {
			inExplosion = false;
		}
		if (localName.equalsIgnoreCase("image")) {
			inImage = false;
		}
		if (localName.equalsIgnoreCase("pose")) {
			inPose = false;
			spriteData.poses.add(currentPose);
		}
		if (localName.equalsIgnoreCase("name")) {
			inPoseName = false;
		}
		if (localName.equalsIgnoreCase("bounding-box")) {
			inBoundingBox = false;
			Rect rect = currentBoundingBox.getRect();
			currentPose.boundingBox.set(
					rect.left, rect.top,
					rect.left + rect.right,
					rect.top + rect.bottom);
		}
		if (localName.equalsIgnoreCase("duration")) {
			if (inFrame)
				inFrameDuration = false;
			else
				inPoseDuration = false;
		}
		if (localName.equalsIgnoreCase("repeats")) {
			inPoseRepeats = false;
		}
		if (localName.equalsIgnoreCase("frame")
				|| localName.equalsIgnoreCase("tween")) {
			inFrame = false;
			currentPose.frames.add(currentFrame);
		}
		if (localName.equalsIgnoreCase("x")) {
			if (inFrame)
				inFrameX = false;
			else if (inBoundingBox)
				inBoundingBoxX = false;
		}
		if (localName.equalsIgnoreCase("y")) {
			if (inFrame)
				inFrameY = false;
			else if (inBoundingBox)
				inBoundingBoxY = false;
		}
		if (localName.equalsIgnoreCase("width")) {
			if (inFrame)
				inFrameWidth = false;
			else if (inBoundingBox)
				inBoundingBoxWidth = false;
		}
		if (localName.equalsIgnoreCase("height")) {
			if (inFrame)
				inFrameHeight = false;
			else if (inBoundingBox)
				inBoundingBoxHeight = false;
		}
		if (localName.equalsIgnoreCase("x-magnification")) {
			inFrameMagX = false;
		}
		if (localName.equalsIgnoreCase("y-magnification")) {
			inFrameMagY = false;
		}
		if (localName.equalsIgnoreCase("angle")) {
			inFrameAngle = false;
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		if (inExplosion) {
			String explosionStr = new String(ch, start, length);
			spriteData.explosionId = Integer.parseInt(explosionStr);
		}
		if (inImage) {
			spriteData.image = new String(ch, start, length);
		}
		if (inPose) {
			if (inPoseName) {
				currentPose.name = new String(ch, start, length);
			}
			if (inBoundingBox) {
				Rect rect = currentBoundingBox.getRect();
				if (inBoundingBoxX) {
					rect.left = Integer.parseInt(new String(ch, start, length));
				}
				if (inBoundingBoxY) {
					rect.top = Integer.parseInt(new String(ch, start, length));
				}
				if (inBoundingBoxWidth) {
					rect.right = Integer.parseInt(new String(ch, start, length));
				}
				if (inBoundingBoxHeight) {
					rect.bottom = Integer.parseInt(new String(ch, start, length));
				}
			}
			if (inPoseDuration) {
				currentPose.defaultDuration = Integer.parseInt(new String(ch,
						start, length));
			}
			if (inPoseRepeats) {
				currentPose.repeats = Integer.parseInt(new String(ch, start,
						length));
			}
			if (inFrame) {
				if (inFrameX) {
					currentFrame.x = Integer
							.parseInt(new String(ch, start, length));
				}
				if (inFrameY) {
					currentFrame.y = Integer
							.parseInt(new String(ch, start, length));
				}
				if (inFrameWidth) {
					currentFrame.width = Integer.parseInt(new String(ch, start,
							length));
				}
				if (inFrameHeight) {
					currentFrame.height = Integer.parseInt(new String(ch, start,
							length));
				}
				if (inFrameMagX) {
					currentFrame.xMagnification = Float.parseFloat(new String(ch,
							start, length));
				}
				if (inFrameMagY) {
					currentFrame.yMagnification = Float.parseFloat(new String(ch,
							start, length));
				}
				if (inFrameDuration) {
					currentFrame.duration = Integer.parseInt(new String(ch, start,
							length));
				}
				if (inFrameAngle) {
					currentFrame.angle = Integer.parseInt(new String(ch, start,
							length));
				}
			}
		}
	}

	/**
	 *
	 * @return The sprite that was read.
	 */
	public SpriteData getSpriteData() {
		return spriteData;
	}
}
